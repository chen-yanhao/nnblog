# UIM任务细分

# 任务列表和知识点

| 编号 | 流程         | 控制器                   | 知识点和用例                                                 |
| ---- | ------------ | ------------------------ | ------------------------------------------------------------ |
| 1    | 登录流程     | user.servlet?param=login | 基本的MVC设计模式、Session和Cookie                           |
| 2    | 注册流程     | user.servlet?param=reg   | AJAX操作和事务控制                                           |
| 3    | 用户信息管理 | user.servlet             | 新增用户流程、删除用户流程、修改用户流程、查询流程、分页流程，用户角色修改流程 |

上图的编号1，2，3三个任务共同组成了用户信息管理模块。该模块涉及到的表包括：uim_user、uim_user_detail、uim_hobby、uim_np。

相关数据表的结构设计见uim_init.sql文件中的内容。

# 1，登录流程

## (1)登录流程的具体实现

请求（request）流程：

login.jsp-->user.servlet?param=login-->UserServlet-->UserServcieImpl-->UserJdbcDaoImpl-->MySQL;

响应（response）流程：

MySQL-->UserJdbcDaoImpl-->int-->UserServiceImpl-->true/false-->UserServlet-->views/main.jsp or login.jsp

### login.jsp

#### EL表达式

login.jsp页面，任务是和用户交互，此处用到了form表单中的action；EL语言`${...}`。

EL的作用就是在页面中显示后台的数据。

```jsp
 <nav id="errorMsg">${requestScope.message}</nav>
```

以上代码在页面中的显示了一个文本信息，其对应的服务端写法如下：

```java
request.setAttribute("message","用户名或密码错误！");
```

EL是JSP原生的表达式，可以出现在JSP文件的任何地方。实际上大部分web模板都支持EL。

#### 页面指令

每个JSP文件的开头都必须是这行代码。一般来说现代的JSP技术只应该有这一句指令代码。

```jsp
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
```

### UserServlet

Servlet控制器，继承了HttpServlet类或实现了Servlet接口的普通Java类。

#### Servlet3.0+版本的映射控制

在MVC设计模式中Servlet是控制器，作用是隔离同时连接M端和V端。同样是类，Servlet是如何让V端可以访问到它的？

这里使用是映射控制，所谓的映射控制就是不使用类名或对象名，而使用一种别名机制在V端对Servlet进行请求。从Servlet3.0开始，这种映射完全使用注解的方式，如下：

```java
@WebServlet(urlParttens={"test.servlet"})
```

需要注意的是：以上注解只能放在类的声明的上面。

Servlet一般包含两个关键方法：

```java
protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException;

protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException;
```

解析：以上方法由容器自动调用，其中参数自动注入。两个方法分别对应method="post"和method=“get”的提交操作。

Servlet中不应该做超出其本职工作的任何操作，其本职工作包括：

1，从前端获取数据，如果数据比较大，则在本地进行封装。

2，调用相应的模型，并获取模型的返回数据。

3，根据模型的返回数据给前端返回数据（可能是页面，也可能就是一般的数据），从而完成一个完整的流程。

### 同步提交时向客户端返回页面

存储转发

```java
request.getRequestDispatcher("login.jsp").forward(request, response);
```

重定向

```java
response.sendRedirect("login.jsp");
```

存储转发和重定向的区别：

存储转发是服务器内部的行为，和客户端无关。最明显的效果的是页面显示的是一个jsp文件，但地址栏依然是某个Servlet。存储转发可以保留request、response等对象。

重定向实质上是两次请求，这两请求和客户端相关。所以重定向时，地址栏会发生变化。重定向无法保留request、response对象。

### 异步提交时向客户端返回数据

响应异步提交时，一般不返回页面，也就不使用存储转发或重定向；而是只返回数据。

目前在前、后端之间传输数据的主要格式是JSON。所以一般来说需要将数据装载在到Map对象中，并将该对象转换成JSON字符串，利用PrintWriter的实例进行发送。

数据发送完成后，就可以激活前端的readystatechange事件，并且将readyState属性设置成4，stauts属性设置为200。

这样一个完整的基于异步提交的流程就完成了。

### UserServiceImpl

Service部分专门处理业务逻辑，所有的复杂的业务代码都在这里编码，在登录操作中，由于没有复杂操作，所以Service部分中只是简单的调用。有时这部分可以省略。

大多数情况下，复杂业务的事务操作都在该层进行。

一条SQL语句在执行时天然就是ACID（原子性、隔离性、一致性、持久性）的。但是当一个具体的流程中需要多次执行SQL语句的时候，ACID就没法保证了。此时就需要使用事务控制。而事务控制的代码一定要的业务层进行。

详细的说：一个流程中的多次SQL语句，必须放在Service中的某一个方法内部。以此保证多条SQL语句的ACID。在之后使用Spring的时候，会将Service层中的方法进行AOP化。从而形成对方法包裹，以此完成事务控制。

## (2)用户认证问题

### Session技术

sessionId是Session技术的起源，sessionId是服务器给每个访问该服务器的用户分发的一个由英文字符和数字组成的字符序列。服务器用这个字符序列来区分每一个客户端。

sessionId的特性是每一个用户的sessionId都不同，默认情况下，当某个用户和服务器超过30分钟没有交流后，sessionId失效。

Session技术利用了sessionId，在服务器端为每个用户准备了一个存储空间，用户只能通过对应的sessionId来访问这个存储空间。在Java中，这个存储空间被抽象成了session对象。session对象实际上是HttpSession类的实例。

### 统一认证问题（filter）

通过filter来解决session的统一认证问题，利用“/views/*”这样的匹配策略，将所有对views目录的访问全部加上CheckSessionFilter过滤器。该过滤器需要追踪request和forward流。其中request流是请求流，一般是jsp转向servlet的流，也有可能是jsp转向另一个jsp的流；forward流则请求转发中的转发流，一般都是转身一个jsp。

filter

filter是servlet同级别的Java Web类。filter是负责过滤请求流和响应用流。

filter的写法

```java
@WebFilter(dispatcherTypes = {DispatcherType.REQUEST, DispatcherType.FORWARD
}, urlPatterns = { "/*" })
public class CharsetFilter implements Filter {
    public void destroy() {}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {}
}
```

## (3)Cookie问题

cookie是服务器发送给客户端的小礼物（cookie是小糕点的意思），其本质上是存储在客户端的一个文本文件。

本项目中，在UserServlet中创建了Cookie，并发送给了客户端；在InitServlet中对这个Cookie进行的校验。以下代码均来自于这两个类中。

通过以下方法创建并发送给客户端：

```java
Cookie cookie=new Cookie("loginCookie", userName);//创建一个cookie
cookie.setMaxAge(10);	//设置cookie的有有效时间,时间以秒为单位。
response.addCookie(cookie);	//利用response向客户端发送cookie
```

通过以下方法提取出相应的内容：

```java
Cookie[] cookies=request.getCookies();	//提取当前客户端中所有的cookie
if(null!=cookies) {
    for(Cookie cookie:cookies) {
        if(cookie.getName().equals("...")) {
			...;
            break;
        }
    }			
}
```

解析：

1，提取cookie的时候，是不能单独提取，只能一块都拿出来。然后通过比对，查找相应的cookie。

2，其中cookie.getName()是获取某个cookie的键；而cookie.getValue()是获取某个cookie的值。

### Optional类

Optional类是专门处理null和可能的npe异常的类，出现在JDK8中。

初始化部分

```java
public static <T> Optional<T> ofNullable(T value)	//创建一个Optional类的实例，可以接收一个值，该值可以为null。
public static <T> Optional<T> of(T value)	//创建一个Optional类的实例，可以接收一个值，该值不能为null。为null时直接报npe。
public static<T> Optional<T> empty()	//创建一个包含null的Optional类的实例
```

解析：Optional类也可以是普通的构造器创建，其中空参的构造器会创建一个包含null的实例；而有参的构造器会创建一个包含该参数的实例。

取值部分

```java
public T get()	//返回内部的值，如果内部的值为null，则抛出NoSuchElementException异常。该方法不争议使用。
public T orElse(T other)	//返回内部的值，如果内部的值为null，就返回other。
public T orElseGet(Supplier<? extends T> other)	//返回内部的值，如果内部的值为null，则运行函数并返该函数返回值
public void ifPresent(Consumer<? super T> consumer) //当其中的值不为空时，执行函数化参数；当其中的值为null时，什么都不做。
```

例子：

```java
user=Optional.ofNullable(list.size()!=0?list.get(0):null);	//赋值

optional.orElse(new User());	//返回一个值，如果为null则new一个对象
optional.orElseGet(()->new User()).getUserName();	//返回一个值，如果为null则new一个对象。
optional.ifPresent((user)->System.out.println(user.getUserName()));	//有值时输出，无值时什么也不做。
```

# 2，注册流程

## Maven中不能进行Jetty热部署的问题

Jetty是可以进行热部署的，但idea默认不进行自动编译，所以这种热部署无效。需要设置idea为自动编译。具体步骤如下：

settings->Build,Execution,Deployment->complier选项中勾选"Build project automatically"选项。

## request.getParameterValues()方法

该方法负责从客户端获取复合数据，大部分情况下是复选框的数据。其提交形式如下：

```
...?hobbies=1&hobbies=2&hobbyies=5
```

该方法返回的是一个字符串数组。其原型如下：

```java
public String[] getParameterValues(String name)
```

# 3，用户信息管理（CURD）

## 1，用户信息表的初始化（user.servlet?param=init）

用户信息表的初始化主要是一个数据表的连接操作：使用users表分别和userDetail表及nativePlaces表进行连接操作。形成一个大表。这里使用MySQL的左外连接操作，即left outer join ... on。

由于返回的是一张大表，在domain是不可能有这张大表的模型的。所以需要使用List<Map<String,Object>>返回集合，存储这张大表的数据，List中的每个元素，代表大表中的一行；Map中的每个键值对，代表当前行中的表头和相对应的数据。

对大表中的数据进行优化以，以更好的显示给用户。优化部分为：性别的0，1，2分别改为null，男，女；行政区划信息，由3701，改为“山东，济南”；爱好信息由1,2,3，改为“音乐、运行、调程序”；这就需要在Serivce层中进行业务代码编写和控制工作。

## 2，用户信息的删除（user.servlet?param=delete）

### 删除时的具体操作

一般操作如下：

```java
String sql="delete from users where id in (?)";
```

这种操作在MySQL中有问题，MySQL无法进行隐式类型转换，而传进来的参数必须是字符串（由于中间可能有逗 号），所以无法使用以上代码。解决方案如下：

```java
String sql="delete from users where id in ("+ids+")";
```

但是这种解决方案将面临SQL注入式攻击的危险。

### 删除时的外键约束问题

删除用户信息时，如果删除的用户是有对应的用记详情的的数据时，由于外键约束的存在，所以无法删除。报如下错误：

```
Cannot delete or update a parent row: a foreign key constraint fails (`umm`.`userdetail`, CONSTRAINT `userdetail_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`))
```

这就需要首先判断userDetail表中是否有相关数据，如果有的话，要删除userDetail表中的数据。达到此目的有两种方法：

1，利用数据逻辑，如触发器，在删除user表中内容之前，先删除userDetail表中的相应数据。

2，利用业务逻辑，即在java直接解决该问题。

现代编程思想更倾向于第二种方案，因为数据逻辑的迁移比较麻烦，且业务逻辑的速度已经跟上来了。

### 利用业务逻辑处理用户删除的步骤

1，获取要删除的用户的id的集合ids，此id为users表中的id。

2，利用ids中的每一个id，去userDetail表中查询该id（在userDetail表中该id为userId）对应的id。获取userDetail表中相应id的集合。

3，根据userDetail表中的id集，删除该表中的相应行。

4，删除users表中的相应行。

## 3，用户信息修改的设计原理

修改操作实际是用户参与的两次操作数据库，第一次是一个查询，查询相应id所对应的行中的所有数据；之后将该数据显示在页面中，拱用户修改；修改完成后点击提交，是修改的第二步操作，即真正的修改操作。涉及到如下两个流程：

user.servlet?param=update，实际上是一个查询流程

user.servlet?param=doUpdate，真正的修改操作流程

修改操作中间的给用户显示要修改的数据的页面，有两种情况：

1，单独打开一个页，显示用户可以修改的数据；

2，在当前页面，利用局部的SPA操作，显示用户可以修改的数据。这种操作就要涉及到一些复杂的、基于JS的DOM操作。同时user.servlet?param=update流程要进行AJAX提交。

## 4，基于局部SPA的用户信息获取

整个用户信息管理页面，都是基于局部SPA的。即CRUD都在一个页面进行。基于局部SPA操作需要进行复杂的DOM操作，从而利用DOM生成页面中没有，但可以从后台获取的各种数据。

从后台获取数据：

```java
if("update".equals(param)) {
    Map<String,Object> uaud=userService.queryUserAndUserDetailById(Integer.valueOf(id));
    //获取并按照用户特性修改数据
    List<Hobby> hobbiesVo=hobbyService.showHobbies();
    //获取已选择的爱好
    for(Hobby hobby:hobbiesVo) {
        String temp=(String)uaud.get("hobby_code");
        if(null!=temp&&temp.indexOf(String.valueOf(hobby.getCode()))!=-1) {
            hobby.setChecked(true);
        }
    }
    List<NativePlace> provinces=nativePlaceService.getProvince();
    List<NativePlace> cities=nativePlaceService.getCities();
    if(null!=uaud) {
        map.put("result", "ok");
        map.put("userAndUserDetail",uaud);
        map.put("hobbies",hobbiesVo);
        map.put("provinces",provinces);
        map.put("cities",cities);
    }else {
        map.put("result", "error");			
    }
}
```

前端利用JS完成复杂的DOM操作

```js
function updateInit(response){
	let json=response.data;
	if(json.result=="ok"){
        //基础性数据展示，Input标签的展示
        document.getElementById("userTable").hidden = true;
        document.getElementById("userOperation").hidden = false;
        document.getElementById("operationMsg").innerHTML = "->&nbsp;修改用户";      
        document.getElementById("userName").value=json.userAndUserDetail.userName;
        document.getElementById("nickName").value=json.userAndUserDetail.nickName;
        document.getElementById("password").value=json.userAndUserDetail.password;
        document.getElementById("email").value=json.userAndUserDetail.email;
        document.getElementById("phone").value=json.userAndUserDetail.phone;
        //input标签中的日期类型展示，注意：日期类型的月、日为单数时，需要在其前面加零以凑成双数。
        //此处引用了WebUtils类中的getDate()函数进行操作。
        let strDate=new WebUtils().getDate(json.userAndUserDetail.birthday);
        document.getElementById("birthday").value=strDate;
        //对单选框的操作
        if(json.userAndUserDetail.sex==1){
        	document.getElementById("man").checked=true;
        }else if(json.userAndUserDetail.sex==2){
        	document.getElementById("woman").checked=true;
        }
        //对复选框的操作
        let hobby=document.getElementById("hobbyInsertPoint");
        hobby.length=0;
        json.hobbies.forEach(element=>{
        	let input=document.createElement("input");
        	input.type="checkbox";
        	input.value=element.code;
        	if(element.checked){
        		input.checked=true;
        	}
        	hobby.appendChild(input);
        	hobby.appendChild(document.createTextNode(element.name))
        });
        //对省份下拉列表框的操作
        let province=document.getElementById("province");
        province.length=0;
        let currentProvinceCode=json.userAndUserDetail.nativePlace_code.substring(0,2);
        json.provinces.forEach(element=>{
        	let option=document.createElement("option");
        	option.value=element.code;
        	option.innerHTML=element.name;
        	if(element.code==currentProvinceCode){
        		option.selected=true;
        	}
        	province.appendChild(option);
        });
        //对城市下拉列表框的操作
        let city=document.getElementById("city");
        city.length=0;
        json.cities.filter(element=>currentProvinceCode==element.code.substring(0,2))
         .forEach(element=>{
        	let option=document.createElement("option");
        	option.value=element.code;
        	option.innerHTML=element.name;
        	if(element.code==json.userAndUserDetail.nativePlace_code){
        		option.selected=true;
        	}
        	city.appendChild(option);            	
        });
	}else{
		alert("数据查询错误！");
	}
}
```

## 5，提交修改后的内容

用户修改也涉及到两张表的操作：users表和userDetail表。所以也需要事务处理。

### 编码中遇到的主要问题

1，MySQL的update语句和delete语句的问题。delete子句必须加from关键字；而update必须不加。这两点和标准SQL稍有不同。

2，数据格式问题，主要数据为空的问题。数据质量问题需要进一步的重视。由于前端检验比较严格所以通过前端上传的数据一般没有问题；数据质量问题主要出现在通过SQL客户端提交的一些实验数据。尤其是userDetail中一行数据没有userId。这和表结构设计时不足也有关系，userId没设计成不能为空的约束。

## 6，分页

### 基于MySQL的分页代码和公式

limit子句，语法如下：

```sql
select * from users limit n,m;
```

解析：

1，n是一个正整数，指的是要显示的页的第一行的前一行。

2，m是一个整数，指的是每页可以显示的行数。

分页公式

（前当页数-1）*每页行数=前当页的第一行数据的前一行的游标位置。所n值应该是这个公式的值。

### 具体的分页操作

1，修改user.servlet?param=init流程中的表数据查询代码，在Dao层中的相就方法中的SQL语句中加入limit子句。相应的修改Service层、Servlet中的代码。

2，在领域模型中加入`Page<T>`类，作为分页信息的综合储存类。

```java
public class Page<T>{
	private int currentPage;						//当前页数，分页的核心变量
	private int pageNumber=Constants.PAGE_NUMBER;	//每页行数，常量
	private int totalRowNumber;						//总行数，通过总行数获取总页数
	private int totalPageNumber;					//总页数，同时也是尾页页数
	private T list;									//每页的数据
}
```

以上两步所涉及到的最核心的变量是currentPage和pageNumber，这两个变量都存放在Page类中。

3，在页面中实现currentPage参数的传递操作。

```jsp
<!--在页面中存储当前页数-->
当前第<span id="currentPage">${requestScope.page.currentPage}</span>页
```

利用事件的冒泡传递效应，在所有分页a标签外，捕获单击事件，利用event.target.id获取事件源的id，并能过对id的匹配，对分页的不同指令进行操作。具体操作如下：

```js
function pageOpation(event){
	console.log("event id is:"+event.target.id);
	let pageTag=event.target.id; 
	let currentPage=document.getElementById("currentPage").innerHTML;	//提取当前页数
    //根据pageTag的值，对当前页数进行相应的操作
	switch(pageTag){
		case "next":currentPage++;break;
		case "first":currentPage=1;break;
		case "last":break;
		case "previous":currentPage--;break;
	}
	window.location.href="user.servlet?param=init&currentPage="+currentPage;
}
```

4，利用下拉列表选择页数

```jsp
<select id="jumpPage" class="normal-input" style="width:50px;height:30px;">
    <c:forEach begin="1" end="${requestScope.page.totalPageNumber }" var="i">
        <c:if test="${i==requestScope.page.currentPage}">
            <option selected  value="${i }">${i }</option>
        </c:if>
        <c:if test="${i!=requestScope.page.currentPage}">
            <option value="${i }">${i }</option>
        </c:if>
    </c:forEach>
</select>
```

## 7，基于分页的简单查询

难点分析：简单的查询和分页本身都不难，关键是如何将二者组合在一起。其表现就是当查询到的数据超过一页时，点击下一页如何使显示的数据依然是查询后的结果。

解决方案：第一次查询的时候，将keyword关键字提取到后端，并进行相关的操作；之后才把该关键字返回给前端；前端在执行pageOperation()函数时，检查keyword是否为空，如果不空，在分页转向时，带上该关键字。

前端分页执行函数

```js
function pageOpation(event){
	console.log(event.type+","+event.target.id);
	let pageTag=event.target.id;
	let type=event.type;
	let currentPage=document.getElementById("currentPage").innerHTML;
	let keyword=document.getElementById("keyword").value;
	console.log("keyword:"+keyword);
	let url="user.servlet?param=init&";
	if(keyword!=""){
		url+="keyword="+keyword+"&"
	}
	if("jumpPage"!=pageTag && "click"==type){
		switch(pageTag){
			case "next":currentPage++;break;
			case "first":currentPage=1;break;
			case "last":currentPage=document.getElementById("totalPageNumber").innerHTML;break;
			case "previous":currentPage--;break;
		}
		window.location.href=url+"currentPage="+currentPage;
	} 
	if("jumpPage"==pageTag && "change"==type){
		currentPage=document.getElementById("jumpPage").value;
		window.location.href=url+"currentPage="+currentPage;
	}
}
```

后端Dao的具体操作

```java
public void queryAllUserAndUserDetailByPage(Page<List<Map<String,Object>>> page) {
    String sql="select u.id,u.userName,u.nickName,u.password,u.sex,"
        +"u.email,u.phone,np.name,np.name,ud.hobby_code,ud.birthday,ud.nativePlace_code "
        +"from users u left outer join userDetail ud on u.id=ud.userId "
        +"left outer join nativeplaces np on ud.nativePlace_code=np.code where 1=1 ";
    if(!"".equals(page.getKeyword())) {
        sql+="and u.userName like ?";
    }
    sql+= " limit ?,?";
    int beginPage=(page.getCurrentPage()-1)*page.getPageNumber();
    int rowLength=page.getPageNumber();
    String keyword=page.getKeyword();
    ResultSetHandler<List<Map<String,Object>>> rsh=new ListMapHandler();
    List<Map<String,Object>> list=null;
    if("".equals(keyword)) {
        list=JdbcTemplate.query(sql,rsh,beginPage,rowLength);		
    }else {
        list=JdbcTemplate.query(sql,rsh,"%"+keyword+"%",beginPage,rowLength);			
    }
    page.setList(list);
} 
```
