# 用户管理模块（UMM）项目教学手册

##						——前端与Java Web技术综合项目

# 教学计划

第一周、第二周：前端技术串讲+UMM项目前端部分编码。

第二周：MySQL基础串讲+UMM项目JDBC封装编码。

第三周：Java Core技术串讲

第四周、第五周、第六周：Java Web技术串讲+UMM项目后台技术编码。

第七周：Spring技术串讲、SpringMVC技术串讲+项目配置

第八周：Spring Boot技术串讲+项目配置

每周1，2，3，5上课，每天7节课。

# 一，概述

## 1，技术路线

### 传统技术路线

目标：Java web工程师

技术栈：Java语言；数据库（Oracle或MySQL）;一般的前端技术（HTML、CSS、JavaScript）;JSP/JSTL、Spring、MyBatis框架类技术等。

### 前端技术路线

目标：前端工程师（技术岗）

技术栈：数据库（NoSQL）;高级的前端技术（HTML、CSS、JavaScript\TypeScript）;vue、react等框架技术。

## 2，UMM项目所处的位置

较老的、传统技术路线的项目。

前端使用使用一般的HTML、CSS、原生的JavaScript技术；数据库使用MySQL；后端使用JSP/Servlet、JSTL/EL。

前后端不分离（前后端分离是一种大的发展趋势）



# 三，数据库、Java基础任务（MySQL）

## MySQL基础

MySQL主发应用在互联网企业中，原因是MySQL的社区是免费的。使用MySQL的主要问题是没有厂家的技术支持，出了问题必须由自己的技术人员解决。由于出现了大量的MySQL的大神级的人物。

软件安装（略）

MySQL的技术参数

1，MySQL5.7版本，目前的最新版是MySQL8版本。

2，数据引擎：InnoDB（MySQ目前的默认引擎）

## 数据库设计和创建

使用PowerDesigner软件设计数据库的物理模型。基本参照如下：

1，用户信息管理模块：用户表、用户详情表、行政区划表、爱好表
2，用户权限管理模：角色表、权限表、用户角色表、角色权限表

要求完成基本的设计工作，生成pdm文件。

### 创建数据库和用户

```sql
create database umm character set=utf8mb4;
use umm;

CREATE USER 'umm'@'%' IDENTIFIED BY 'umm';
GRANT privileges on umm.* to 'umm'@'%';
```

### 创建用户信息相关表及有关的原理性问题

创建users、hobby、nativePlace、userDetail表为其添加基本数据。

#### 知识点分析

数据库创建、用户创建、表的创建、约束等操作的编码实现。

修改表数据结构的语法

表间关系和外键实现

#### 具体实现代码

```mysql
CREATE DATABASE umm CHARACTER SET=utf8mb4;	-- 创建一个名为umm的数据库，设置其编码标准为utf8mb4(utf8宽字节标准)
USE umm;	-- 使用umm数据库
/*
在MySQL中，database相当于Oracle12c之前的Oracle中的tablespace。
*/
CREATE USER 'umm'@'%' IDENTIFIED BY 'umm';	-- 创建用户umm，密码umm，并允许用户通过任何方式访问
GRANT ALL ON umm.* TO 'umm'@'%';			-- 赋予umm用户有关umm数据库的所有权限
USE umm;
/*
基础的用户表，存储最基本的用户信息
*/
CREATE TABLE users(
	id INT AUTO_INCREMENT PRIMARY KEY,	-- 代理主键，自增
	userName VARCHAR(50) NOT NULL,
	nickName VARCHAR(100) NOT NULL,
	PASSWORD VARCHAR(50) NOT NULL,
	sex char(1) default '1',			-- 注意，这是一个新增字段
	email VARCHAR(100) NOT NULL,
	photo VARCHAR(20) NOT NULL
);
SELECT * FROM users;

show tables;	-- 查看当前数据库中所有的表的信息
desc users;		-- 查看users表的表结构

/*
在表结构生成为添加一个字段
*/
alter table users add sex char(1) default '1';
/*
将某个字段在表结构中的位置进行调整，after关键子和first关键字
*/
alter table users modify sex char(1) after password;

TRUNCATE TABLE users;
/*
面试题1：delete与truncate的区别？
答两个关键字都是删除表中的数据，delete可以删除部分数据，但效率低；truncate只能删除表中的全部数据，但效率高；truncate会导致自增数据归零。

面试题2：modify与change的区别？
答：change可以修改列名和列属性；modify只能修改列属性、列的位置和约束。
语法：
alter table tableName change oldColumnName newColumnName type;
alter table tableName modify columnName type [first|after] columnName;
*/
alter table users change userName userName1 varchar(55);	--修改userName为userName1，同时修改类型
alter table users modify userName1 varchar(50);				--修改userName1的类型
alter table users change userName1 userName varchar(50);	--修改userName1为userName。

-- 批量插入数据
INSERT INTO users(userName,nickName,PASSWORD,sex,email,photo) VALUES
('admin','一般家庭马化腾','admin','1','admin@qq.com','12344'),
('test','悔创阿里Jack马','test','1','test@qq.com','234234'),
('manager','一无所有王健林','manager','1','manager@qq.com','34343'),
('leader','北大还行萨贝宁','leader','1','test@qq.com','12334343');

-- 创建爱好数据表
CREATE TABLE hobby
(
   id                   INT AUTO_INCREMENT PRIMARY KEY,
   NAME                 VARCHAR(50),
   CODE                 INT
);
alter table hobby modify code int after id;
alter table hobby modify code int not null;
alter table hobby modify name varchar(50) not null;
INSERT INTO hobby(NAME,CODE) VALUES('音乐',1),('运动',2),('调程序',3),('看书',4),('敲代码',5);
SELECT * FROM hobby;

--创建行政区划表
CREATE TABLE nativePlaces
(
   id                   INT PRIMARY KEY AUTO_INCREMENT,
   NAME                 VARCHAR(50),
   CODE                 VARCHAR(6) UNIQUE
);
/*unique关键字，候选键约束*/

INSERT INTO nativePlaces(NAME,CODE) VALUES('山东','37'),('济南','3701'),('青岛','3702'),('江苏','32'),('南京','3201'),('江苏','3202');
SELECT * FROM nativePlaces;

-- 创建用户详情表
CREATE TABLE userDetail
(
   id                   INT PRIMARY KEY AUTO_INCREMENT,
   nativePlace_code     VARCHAR(6),
   hobby_code           VARCHAR(50),
   userId               INT unique,
   birthday             DATETIME,
   FOREIGN KEY(userId) REFERENCES users(id),	--一对一外键
   FOREIGN KEY(nativePlace_code) REFERENCES nativePlaces(CODE)	--一对多外键
);
alter table userDetail modify userId int unique;
/*
表间关系：一对一关系；一对多关系；多对多关系
其中多对多关系不能用外键实现，一般是把两个表的多对多关系，拆成三个表的两对一对多关系。
拆法是把两个表的外键字段加上一个新代理主键生成一个新表。
一对一关系和一对关系的外键实现
一对一关系的外键实现：两种情况：
1，辅表的主键对应主表的主键
2，辅表的候选键对应主表的主键（一对多关系的特殊形态）
例子：users，userDetail
FOREIGN KEY(id) REFERENCES users(id)		--这是标准的一对一关系
FOREIGN KEY(userId) REFERENCES users(id)	--本质上是一对多关系，但是由于userId是unique，所以也成了一对一关系
*/


DROP TABLE userdetail;
SELECT * FROM userdetail;

SHOW TABLES;

INSERT INTO userDetail(nativePlace_code,hobby_code,userId,birthday) VALUES('3701','1,2,3',1,'1972-8-4');
INSERT INTO userDetail(nativePlace_code,hobby_code,userId,birthday) VALUES('3702','1,3,5',2,'1980-8-4'),
('3202','3,5',3,'1981-8-4'),('3701','3,5',4,'1991-8-4');
```

## Java Core技术串讲

Java基础、编程逻辑串讲

OOP串讲

数组、字符串及相关工具类串讲

异常串讲

集合串讲

# 四，后端任务

## 软件架构

最基础的MVC模式：JSP+Servlet+JavaBean结构，使用maven构建和管理，使用git进行版本控制。

使用JSTL/EL、原生AJAX等技术。

### 1，引入log4j日志系统

首先在pom.xml文件中添加依赖

```
<dependency>
    <groupId>log4j</groupId>
    <artifactId>log4j</artifactId>
    <version>1.2.17</version>
</dependency>
```

之后添加log4j.properties配置文件

```
log4j.rootLogger=debug,console

log4j.appender.console=org.apache.log4j.ConsoleAppender
log4j.appender.console.layout=org.apache.log4j.PatternLayout
log4j.appender.console.layout.ConversionPattern= %d - %c -%-4r [%t] %-5p - %m%n
```

最后在每个要进行日志输出的类中的加入Logger的声明。如下：

```java
private static Logger log=Logger.getLogger(DBConnection.class);
...
log.debug("....");
log.error("....");
```

### 2，事务控制处理

增加了事务控制类，为此要对数据库连接类进行一些修改。

主要的目的是实现线程和connection引用对象的统一。关键是使用ThreadLocal类。ThreadLocal类的数据结构是Map。键是线程id，值可以放任何对象。

## 主任务细分列表

### 任务列表和知识点

| 编号 | 流程         | 流程编号      | 控制器                   | 知识点和用例                                                 |
| ---- | ------------ | ------------- | ------------------------ | ------------------------------------------------------------ |
| 1    | 登录流程     | user_login    | user.servlet?param=login | 基本的MVC设计模式、Session和Cookie                           |
| 2    | 注册流程     | user_register | user.servlet?param=reg   | AJAX操作和事务控制                                           |
| 3    | 用户信息管理 | user          | user.servlet             | 新增用户流程、删除用户流程、修改用户流程、查询流程、分页流程，用户角色修改流程 |
| 4    | 角色信息管理 | role          | role.servlet             | 新增角色流程、删除角色流程、修改角色流程、查询流程、分页流程，角色权限修改流程 |
| 5    | 权限信息管理 | permission    | permission.servlet       | 新增权限流程、删除权限流程、修改权限流程、查询流程、分页流程 |

### 4，角色信息和权限管理（CURD）

#### 1，roleManager.jsp和permissionManager.jsp页面及相关操作

##### 代码复用

userManager.jsp中的部分内容实际上在所有的表格显示的页面中都可以复用。所以roleManager.jsp中的相关内容可以直接复用原来userManager.jsp中的部分。主要可以复用的部分如下：

1，标准表样式和针对标准表的操作

normal-table.css

tableOperation.js

2，分页部分的JS操作部分

Page.js

JS部分的复用需要模块化操作才能完成，该项目引入了ES6中的原生模块化应用，即：import和export关键字。同时还使用ES6新的类语法糖：class关键字。

##### CRUD的实现

基上可以复用用户信息管理中的内容，相对于用户信息管理部分也较为简单。

#### 2，角色信息管理中的复杂操作

##### 对某个角色中的权限进行修改的实现

##### 对某个用户的角色进行修改的实现

主要的难点是如何分辨某个角色中的已有权限和为其加入的新权限，解决方案是：不管其原来有什么权限，全部删除，重新为其赋予全新的权限。核心业务代码如下：

```java
public boolean setPermissionRole(int roleId, String[] permissionIds) {
    boolean flag=false;
    TransactionManager tm=new TransactionManager();
    try {
        tm.beginTransaction();
        //删除该角色已有权限，这个删除操作可以不执行，即该角色原来没有任何权限
        permissionRoleDao.deleteData(roleId);	
        //给该角色赋予新的权限
        if(permissionRoleDao.insertData(roleId, permissionIds)>0) {
            flag=true;
        }			
        tm.commit();
    } catch (SQLException e) {
        tm.rollback();
        log.error("setPermissionRole error,error message is "+e.getMessage());
        throw new DaoException();
    }
    return flag;
}
```

##### 选中一级权限会同时选中该权限下的所有权限

未实现

#### 5，用户权限过滤

根据用户的id号，对其可以拥有的权限进行过滤，过滤操作本身基于filter实现。

##### SQL语句

其关键部分是实现通过用户id获取相应的权限，这需要四个表的连接操作。如下：

users----userrole---permissionrole-permission

以上四个表进行基于内连接的表的连接操作，从而找到某个用户对应的权限。

##### 获取用户相应的权限后的操作

由于获取都是子权限，所以还需要添加相应的父权限。也就是说只要有一个子权限，该子权限对应的父权限就应该在列表中。具体操作如下：

``` java
List<Permission> list=permissionDao.queryPermissionByUserId(id);	//获取用户对应权限
List<Permission> parentList=permissionDao.queryParentPermissions();	//获取所有父权限
parentList.forEach(parentPermission->{
    for(Permission p:list){
        if(p.getPid()==parentPermission.getId()){
            list.add(parentPermission);			//将某个父权限插入到用户对应权限列表中
            break;
        }
    }
});
return list;
```

