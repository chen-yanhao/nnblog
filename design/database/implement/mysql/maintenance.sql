create table sys_onlines
(
   id                   int not null,
   user_name             varchar(50),
   user_id               numeric(10,0),
   ip                   varchar(20),
   status               varchar(20),
   date                 datetime,
   primary key (id)
);