-- 用户管理模块（user managerment module）表结构设计

-- 用户权限表
create table umm_permission
(
   id                   int not null primary key auto_increment,
   pid					int not null default 0,	-- 父Id，
   permission_name      varchar(50) not null,	-- 权限名
   url                  varchar(200) not null,	-- 权限的本质
   remark               varchar(100) default ''			-- 权限的说明
);
-- 解析
-- 当此字段为0时，表明是1级权限（菜单）；当此字段为非0时，表明是2级权限（菜单），同时该字段还表明了该2级权限所属的一级权限是谁。
-- 描述
-- 由于本项目是页面粒度的权限管理，也就是对权限的控制到页为止。所以以上的权限表中最关键的是url。即：url就代表权限。
-- 添加主键自增
-- alter table permission change id id int auto_increment;

-- 用户角色表
create table umm_role
(
   id                   int not null primary key auto_increment,
   role_name            varchar(50) not null,		-- 角色名
   remark               varchar(100) default ''		-- 角色说明
);
/*描述
  角色表的出现本身是为应对复杂情况：即权限较多，用户也较多较变化较多。角色表本质上是个容器标识，用来将一部分权限包含起来，
  给用户赋权限的时候，可以按照角色赋予，这样相对方便。

*/

-- 权限角色表
create table umm_permission_role
(
   id                   int not null primary key auto_increment,
   permission_id         int,						-- 外键
   role_id               int,						-- 外键
   foreign key(permission_id) references umm_permission(id),
   foreign key(role_id) references umm_role(id)
);
/*
	描述
	由于权限表和角色表之间是多对多关系，所以无法用一般的外键来实现，必须将两个表的外键提出，生成一个新的表。就是该表。
*/

-- 也可以在建表后添加外键，添加方法如下：
-- alter table permissionrole add foreign key(permissionId) references permission(id);


-- 用户角色表
create table umm_user_role
(
   id                   int not null primary key auto_increment,
   user_id               int,						-- 外键
   role_id               int,						-- 外键
   foreign key(user_id) references uim_user(id),
   foreign key(role_id) references umm_role(id)
);
/*
	描述
	与权限角色表相同
*/




--数据初始化

insert into umm_role(role_name,remark) values('超级管理员','超级管理员角色'),('管理员','管理员角色'),('测试人员','测试人员角色')
,('无权限人员','测试用，不在生产环境中使用');
insert into umm_role(role_name,remark) values('HR','业务角色_人事管理'),('邮件系统管理员','业务角色_邮件管理');


-- 初始化数据库表中的数据
-- 用户权限管理模块权限信息初始化
insert into umm_permission(permission_name,url) values('用户权限管理模块','#');
insert into umm_permission(pid,permission_name,url) values(1,'用户信息管理','user.servlet?param=init');
insert into umm_permission(pid,permission_name,url) values(1,'权限信息管理','permission.servlet?param=init');
insert into umm_permission(pid,permission_name,url) values(1,'角色信息管理','role.servlet?param=init');
-- 邮件管理模块权限信息初始化
insert into umm_permission(pid,permission_name,url) values(0,'邮件管理模块','#');
insert into umm_permission(pid,permission_name,url) values(5,'写邮件','email.servlet?param=writer');
insert into umm_permission(pid,permission_name,url) values(5,'收件箱','email.servlet?param=inbox'),(5,'发件箱','email.servlet?param=outbox');


-- 给角色添加访问用户权限管理模块的权限
insert into umm_permission_role(permission_id,role_id) values(1,1),(2,1),(3,1),(4,1);
-- 解析：给1号角色赋予1，2，3，4号权限。即用户信息管理、角色信息管理、权限信息管理。

-- 给角色添加访问邮件管理模块中写邮件的权限
insert into umm_permission_role(permission_id,role_id) values(5,6),(6,6);

-- 把相应的角色赋予给id为1的用户
insert  into  umm_user_role(user_id,role_id) values(1,1);
insert into umm_user_role(user_id,role_id) values(1,6);


-- 通过用户id获取该用户的所有权限（permission）
select p.id,p.pid,p.permission_name,p.url,p.remark from umm_user u inner join umm_user_role ur on u.id=ur.user_id 
	inner join umm_permission_role pr on ur.role_id=pr.role_id
	inner join umm_permission p on pr.permission_id=p.id where u.id=2;





