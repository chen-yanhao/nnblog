
-- 文章表
create table article
(
   id                   int not null primary key auto_increment,
   title                varchar(200) not null,					-- 标题
   content              text not null,							-- 内容
   publish_time         timestamp default current_timestamp,	-- 发布时间
   click                numeric(10,0)	default 0,				-- 点击量
   like_number          numeric(5,0)	default 0,				-- 点赞
   tags 				varchar(100) 	default null,			-- 标签，形式如：1,2,3
   type_id              int,									-- 类型id
   user_id              int,									-- 用户id
   top                  numeric(3,0)							-- 置顶
);


-- 文章类型表
create table article_type
(
   id                   int not null primary key auto_increment,
   type_name             varchar(50) not null,			-- 类型名
   parent_id             numeric(10,0),					-- 父类型id，该值为0时为根节点
   remark               varchar(100),					-- 说明
   level                numeric(2,0) default 0			-- 级别
);

insert into article_type(type_name,parent_id,remark) values('技术文章',0,'it技术类文章');
insert into article_type(type_name,parent_id,remark) values('心灵硫酸',0,'有毒的心灵鸡汤');
insert into article_type(type_name,parent_id,remark,level) values('前端技术',1,'前端技术类文章',1),('java基础',1,'java core',1);
insert into article_type(type_name,parent_id,remark,level) values('野史',2,'野史类文件',1);


-- 标签字典表
create table article_tag
(
	id 				int not null primary key auto_increment,
	tag_name 		varchar(100) default null		--标签名
)




alter table article add constraint FK_Reference_7 foreign key (typeId)
      references articleType (id) on delete restrict on update restrict;

alter table article add constraint FK_Reference_8 foreign key (userId)
      references users (id) on delete restrict on update restrict;


