
create database nnblog character set=utf8mb4;
use nnblog;

CREATE USER 'nnblog'@'%' IDENTIFIED BY 'nnblog';
GRANT all on nnblog.* to 'nnblog'@'%';