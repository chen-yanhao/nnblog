-- 用户信息管理模块（user information module）表结构和初始化

USE nnblog;
/*
基础的用户表，存储最基本的用户信息
*/
CREATE TABLE uim_user(
	id INT AUTO_INCREMENT PRIMARY KEY,	-- 代理主键，自增
	user_name VARCHAR(50) NOT NULL,
	nick_name VARCHAR(100) NOT NULL,
	password VARCHAR(50) NOT NULL,
	sex char(1) default '0' comment '未选择为0，男为1，女为2',
	email VARCHAR(100) NOT NULL,
	phone VARCHAR(20) NOT NULL
);
SELECT * FROM uim_user;

show tables;	-- 查看当前数据库中所有的表的信息
desc user;		-- 查看users表的表结构

/*
在表结构生成为添加一个字段
*/
alter table uim_user add sex char(1) default '1';

/*
将某个字段在表结构中的位置进行调整，after关键子和first关键字
*/
alter table uim_user modify sex char(1) after password;

TRUNCATE TABLE uim_user;

/*
面试题1：delete与truncate的区别？
答两个关键字都是删除表中的数据，delete可以删除部分数据，但效率低；truncate只能删除表中的全部数据，但效率高；truncate会导致自增数据归零。

面试题2：modify与change的区别？
答：change可以修改列名和列属性；modify只能修改列属性、列的位置和约束。
语法：
alter table tableName change oldColumnName newColumnName type;
alter table tableName modify columnName type [first|after] columnName;
*/
alter table uim_user change userName userName1 varchar(55);	--修改userName为userName1，同时修改类型
alter table uim_user modify userName1 varchar(50);				--修改userName1的类型
alter table uim_user change userName1 userName varchar(50);	--修改userName1为userName。

-- 批量插入数据
INSERT INTO uim_user(user_name,nick_name,password,sex,email,phone) VALUES
('admin','一般家庭马化腾','admin','1','admin@qq.com','12344'),
('test','悔创阿里Jack马','test','1','test@qq.com','234234'),
('manager','一无所有王健林','manager','1','manager@qq.com','34343'),
('leader','北大还行萨贝宁','leader','1','test@qq.com','12334343');

insert into uim_user(user_name,nick_name,password,sex,email,phone) values('test01','....','test01','1','test01@qq.com','123344333');

-- 创建爱好数据表
CREATE TABLE uim_hobby
(
   id                   INT AUTO_INCREMENT PRIMARY KEY,
   name                 VARCHAR(50),
   code                 INT
);
alter table uim_hobby modify code int after id;
alter table uim_hobby modify code int not null;
alter table uim_hobby modify name varchar(50) not null;
INSERT INTO uim_hobby(NAME,CODE) VALUES('音乐',1),('运动',2),('调程序',3),('看书',4),('敲代码',5);
SELECT * FROM uim_hobby;

--创建行政区划表
CREATE TABLE uim_np
(
   id                   INT PRIMARY KEY AUTO_INCREMENT,
   name                 VARCHAR(50),
   code                 VARCHAR(6) UNIQUE
);
-- np is native places
/*unique关键字，候选键约束*/

INSERT INTO uim_np(NAME,CODE) VALUES('山东','37'),('济南','3701'),('青岛','3702'),('江苏','32'),('南京','3201'),('江苏','3202');
SELECT * FROM nativePlaces;

-- 创建用户详情表
CREATE TABLE uim_user_detail
(
   id                   INT PRIMARY KEY AUTO_INCREMENT,
   np_code     			VARCHAR(6),
   hobby_code           VARCHAR(50),
   user_id              INT unique not null,
   birthday             DATETIME,
   FOREIGN KEY(user_id) REFERENCES umm_user(id),	-- 一对一外键
   FOREIGN KEY(np_code) REFERENCES umm_np(code)	-- 一对多外键
);
alter table userDetail modify userId int unique;
/*
表间关系：一对一关系；一对多关系；多对多关系
其中多对多关系不能用外键实现，一般是把两个表的多对多关系，拆成三个表的两对一对多关系。
拆法是把两个表的外键字段加上一个新代理主键生成一个新表。
一对一关系和一对关系的外键实现
一对一关系的外键实现：两种情况：
1，辅表的主键对应主表的主键
2，辅表的候选键对应主表的主键（一对多关系的特殊形态）
例子：users，userDetail
FOREIGN KEY(id) REFERENCES users(id)		--这是标准的一对一关系
FOREIGN KEY(userId) REFERENCES users(id)	--本质上是一对多关系，但是由于userId是unique，所以也成了一对一关系

外键的作用
外键是为了保证数据完整性，即：在删除主表之前，DBMS要求先删除辅表，否则会报错。以此进行数据完整性控制
这种完整性控制也中以交给业务逻辑代码实现，如Java等。

在对效率要求非常高的环境下，外键是不应该加的。
*/

DROP TABLE uim_user_detail;
SELECT * FROM uim_user_detail;

SHOW TABLES;

INSERT INTO uim_user_detail(np_code,hobby_code,user_id,birthday) VALUES('3701','1,2,3',1,'1972-8-4');
INSERT INTO uim_user_detail(np_code,hobby_code,user_id,birthday) VALUES('3702','1,3,5',2,'1980-8-4'),
('3202','3,5',3,'1981-8-4'),('3701','3,5',4,'1991-8-4');

INSERT INTO uim_user_detail(np_code,hobby_code,birthday) VALUES('3701','1,2',STR_TO_DATE('2019-03-20','%Y-%m-%d'));

-- 表的联合查询
/*
左外连接（left outer join）、内连接（inner join）
面试题1：左外连接和内连接的区别？
答：
左外连接是以左表为基础构建查询，左表有而右表没有行，依然存在，其中没有的数据用null填充。
内连接是以两个表为基础构建查询，少一个表的行，忽略。
*/
select * from uim_user;
select * from uim_user u left outer join userDetail ud on u.id=ud.userId;
select u.id,u.userName,u.nickName,u.password,u.sex,u.email,u.photo,ud.nativePlace_code,ud.hobby_code,ud.birthday from uim_user u left outer join uim_user_detail ud on u.id=ud.userId;
-- 左外连接
select u.id,u.userName,u.nickName,u.password,u.sex,u.email,u.phone,np.name nativePlace_code,ud.hobby_code,ud.birthday from uim_user u left outer join uim_user_detail ud on u.id=ud.userId left outer join uim_np np on ud.nativePlace_code=np.code;
-- 右外连接
select u.id,u.userName,u.nickName,u.password,u.sex,u.email,u.photo,np.name nativePlace_code,ud.hobby_code,ud.birthday from uim_user u inner join  uim_user_detail ud on u.id=ud.userId inner join uim_np np on ud.nativePlace_code=np.code;

-- 分页操作

select * from uim_user limit n,m;
/*
解析：
1，n是一个正整数，指的是要显示的页的第一行的前一行。
2，m是一个正整数，指的是每页可以显示的行数。注意：网上到处都说m可以为负数，但通过MySQL的官方文档，可以看到其中要求m必须是正整数。
分页公式
（前当页数-1）*每页行数=前当页的第一行数据的前一行的游标位置。所n值应该是这个公式的值。
*/
-- 用户信息管理界面中主表的分页查询如下：
select u.id,u.user_name,u.nick_name,u.password,u.sex,u.email,u.phone,np.name,np.name,ud.hobby_code,ud.birthday,ud.native_place_code
 from uim_user u left outer join uim_user_detail ud on u.id=ud.userId left outer join uim_np np on ud.native_place_code=np.code
 limit ?,?;
-- 其中第一个？是分页公式的值，第二个？是每页的行数。