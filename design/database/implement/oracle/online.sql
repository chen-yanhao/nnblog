--
create table onlines(
	id number(10) primary key,
	userName varchar2(50) not null,
	ip varchar2(20) not null,
	status varchar2(20) default 'login' not null,
	time date not null
);

create sequence online_id minvalue 0 increment by 1 start with 0;
--yyyy-mm-dd hh:mi:ss
