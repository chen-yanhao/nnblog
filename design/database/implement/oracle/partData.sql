prompt Disabling triggers for ARTICLETYPE...
alter table ARTICLETYPE disable all triggers;
prompt Disabling triggers for HOBBY...
alter table HOBBY disable all triggers;
prompt Disabling triggers for NATIVEPLACE...
alter table NATIVEPLACE disable all triggers;
prompt Disabling triggers for USERS...
alter table USERS disable all triggers;
prompt Disabling triggers for USERDETAIL...
alter table USERDETAIL disable all triggers;
prompt Disabling foreign key constraints for USERDETAIL...
alter table USERDETAIL disable constraint SYS_C0012277;
alter table USERDETAIL disable constraint SYS_C0012278;
prompt Deleting USERDETAIL...
delete from USERDETAIL;
commit;
prompt Deleting USERS...
delete from USERS;
commit;
prompt Deleting NATIVEPLACE...
delete from NATIVEPLACE;
commit;
prompt Deleting HOBBY...
delete from HOBBY;
commit;
prompt Deleting ARTICLETYPE...
delete from ARTICLETYPE;
commit;
prompt Loading ARTICLETYPE...
insert into ARTICLETYPE (id, typename, parentid, url, remark)
values (2, '前端', 0, null, null);
insert into ARTICLETYPE (id, typename, parentid, url, remark)
values (3, '原生JS', 2, 'articleType.servlet?typeId=', null);
insert into ARTICLETYPE (id, typename, parentid, url, remark)
values (4, 'HTML5', 2, 'articleType.servlet?typeId=', null);
insert into ARTICLETYPE (id, typename, parentid, url, remark)
values (5, 'CSS3', 2, 'articleType.servlet?typeId=', null);
insert into ARTICLETYPE (id, typename, parentid, url, remark)
values (6, 'Java', 0, null, null);
insert into ARTICLETYPE (id, typename, parentid, url, remark)
values (7, 'JavaCore', 6, 'articleType.servlet?typeId=', null);
insert into ARTICLETYPE (id, typename, parentid, url, remark)
values (8, 'JavaWeb', 6, 'articleType.servlet?typeId=', null);
insert into ARTICLETYPE (id, typename, parentid, url, remark)
values (9, '框架技术', 0, null, null);
insert into ARTICLETYPE (id, typename, parentid, url, remark)
values (10, 'spring', 9, 'articleType.servlet?typeId=', null);
insert into ARTICLETYPE (id, typename, parentid, url, remark)
values (11, 'mybatis', 9, 'articleType.servlet?typeId=', null);
commit;
prompt 10 records loaded
prompt Loading HOBBY...
insert into HOBBY (id, name, code)
values (20, '刷头条', 7);
insert into HOBBY (id, name, code)
values (1, '学习', 1);
insert into HOBBY (id, name, code)
values (2, '唱歌', 2);
insert into HOBBY (id, name, code)
values (3, '看书', 3);
insert into HOBBY (id, name, code)
values (4, '爬山', 4);
insert into HOBBY (id, name, code)
values (5, '写代码', 5);
insert into HOBBY (id, name, code)
values (7, '调bug', 6);
commit;
prompt 7 records loaded
prompt Loading NATIVEPLACE...
insert into NATIVEPLACE (id, name, code)
values (1, '山东', '37');
insert into NATIVEPLACE (id, name, code)
values (2, '济南', '3701');
insert into NATIVEPLACE (id, name, code)
values (3, '泰安', '3709');
insert into NATIVEPLACE (id, name, code)
values (4, '北京', '01');
insert into NATIVEPLACE (id, name, code)
values (5, '朝阳区', '0101');
insert into NATIVEPLACE (id, name, code)
values (20, '淄博', '3703');
insert into NATIVEPLACE (id, name, code)
values (21, '江苏', '32');
insert into NATIVEPLACE (id, name, code)
values (22, '南京', '3201');
insert into NATIVEPLACE (id, name, code)
values (23, '昆山', '3205');
commit;
prompt 9 records loaded
prompt Loading USERS...
insert into USERS (id, name, nickname, password, sex, email)
values (24, 'test004', 'test004', 'test004', '2', 'test004@qq.com');
insert into USERS (id, name, nickname, password, sex, email)
values (3, 'qqq', 'qqq', 'qqq', '1', 'qqq@qq.com');
insert into USERS (id, name, nickname, password, sex, email)
values (1, 'admin', '一般家庭马化腾', 'admin', '1', 'albert@qq.com');
insert into USERS (id, name, nickname, password, sex, email)
values (2, 'test', '深情的维那斯', 'test', '2', 'john@qq.com');
insert into USERS (id, name, nickname, password, sex, email)
values (6, 'dlzys', 'dlzys', 'lovezq0227211314', '1', '1269969766@qq.com');
insert into USERS (id, name, nickname, password, sex, email)
values (9, 'zzz', '下周回国假药停', '123456', '2', 'zzz@qq.com');
insert into USERS (id, name, nickname, password, sex, email)
values (7, 'aa', 'aa', 'aaaaaa', '1', 'zz@sohu.com');
insert into USERS (id, name, nickname, password, sex, email)
values (4, 'test001', '不识妻美刘强东', 'test001', '2', 'sdfasdf@qq.com');
insert into USERS (id, name, nickname, password, sex, email)
values (20, 'test43210', '悔创阿里Jack马', 'test43210', '1', 'test43210@qq.com');
insert into USERS (id, name, nickname, password, sex, email)
values (23, 'test003', '忠心为国吴三桂', 'test003', '1', 'test003@qq.com');
insert into USERS (id, name, nickname, password, sex, email)
values (41, 'test005', 'test005', 'test005', '2', 'test005@qq.com');
insert into USERS (id, name, nickname, password, sex, email)
values (42, 'test006', 'test006', 'test006', '2', 'test006@sohu.com');
commit;
prompt 12 records loaded
prompt Loading USERDETAIL...
insert into USERDETAIL (id, nativeplace_code, hobby_code, userid)
values (24, '3701', '2,6', 24);
insert into USERDETAIL (id, nativeplace_code, hobby_code, userid)
values (9, '3701', '1,5', 3);
insert into USERDETAIL (id, nativeplace_code, hobby_code, userid)
values (1, '3701', '1,2,3', 1);
insert into USERDETAIL (id, nativeplace_code, hobby_code, userid)
values (13, '3701', '1,5', 7);
insert into USERDETAIL (id, nativeplace_code, hobby_code, userid)
values (10, '0101', '2,6', 4);
insert into USERDETAIL (id, nativeplace_code, hobby_code, userid)
values (23, '0101', '1,5', 23);
insert into USERDETAIL (id, nativeplace_code, hobby_code, userid)
values (41, '3709', '1,4,5', 41);
insert into USERDETAIL (id, nativeplace_code, hobby_code, userid)
values (42, '3701', '1,2,6', 42);
commit;
prompt 8 records loaded
prompt Enabling foreign key constraints for USERDETAIL...
alter table USERDETAIL enable constraint SYS_C0012277;
alter table USERDETAIL enable constraint SYS_C0012278;
prompt Enabling triggers for ARTICLETYPE...
alter table ARTICLETYPE enable all triggers;
prompt Enabling triggers for HOBBY...
alter table HOBBY enable all triggers;
prompt Enabling triggers for NATIVEPLACE...
alter table NATIVEPLACE enable all triggers;
prompt Enabling triggers for USERS...
alter table USERS enable all triggers;
prompt Enabling triggers for USERDETAIL...
alter table USERDETAIL enable all triggers;
