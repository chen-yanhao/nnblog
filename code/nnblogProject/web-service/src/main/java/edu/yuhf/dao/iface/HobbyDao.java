package edu.yuhf.dao.iface;

import java.util.List;

import edu.yuhf.domain.Hobby;

public interface HobbyDao {

	public List<Hobby> getHobbies();
}
