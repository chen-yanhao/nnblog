package edu.yuhf.service;

import java.util.List;

import edu.yuhf.dao.iface.HobbyDao;
import edu.yuhf.dao.mysql.HobbyJdbcDaoImpl;
import edu.yuhf.domain.Hobby;
import edu.yuhf.service.iface.HobbyService;

public class HobbyServiceImpl implements HobbyService{

	private HobbyDao hobbyDao=new HobbyJdbcDaoImpl();
	@Override
	public List<Hobby> showHobbies() {
		return hobbyDao.getHobbies();
	}

}
