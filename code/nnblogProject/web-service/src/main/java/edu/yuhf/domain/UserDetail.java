package edu.yuhf.domain;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDetail {

	private int id;
	private String nativePalceCode;
	private String hobbyCode;
	private int userId;
	private LocalDate birthday;

}
