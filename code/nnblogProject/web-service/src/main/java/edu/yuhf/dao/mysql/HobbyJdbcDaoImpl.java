package edu.yuhf.dao.mysql;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.yuhf.dbutils.JdbcTemplate;

import edu.yuhf.dao.iface.HobbyDao;
import edu.yuhf.domain.Hobby;

public class HobbyJdbcDaoImpl implements HobbyDao {

	private static Logger log=Logger.getLogger(HobbyJdbcDaoImpl.class);
	
	@Override
	public List<Hobby> getHobbies() {
		String sql="select id,code,name from uim_hobby";
		List<Hobby> list= JdbcTemplate.query(sql, (rs)->{
			List<Hobby> list0=new ArrayList<>();
			try {
				while(rs.next()) {
					Hobby hobby=new Hobby(rs.getInt(1),rs.getInt(2),rs.getString(3));
					list0.add(hobby);
				}
			} catch (SQLException e) {
				log.error("rs to List<Hobby> convert error!error message is "+e.getMessage());
			}
			return list0;
		});
		return list;
	}

}
