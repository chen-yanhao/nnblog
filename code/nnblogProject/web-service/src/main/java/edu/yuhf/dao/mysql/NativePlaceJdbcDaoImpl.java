package edu.yuhf.dao.mysql;

import java.util.List;

import com.yuhf.dbutils.JdbcTemplate;

import edu.yuhf.dao.convert.NativePlaceHandler;
import edu.yuhf.dao.iface.NativePlaceDao;
import edu.yuhf.domain.NativePlace;

public class NativePlaceJdbcDaoImpl implements NativePlaceDao {

	@Override
	public List<NativePlace> getProvinces() {
		String sql="select id,code,name from uim_np where length(code)=2";
		List<NativePlace> list=JdbcTemplate.query(sql, new NativePlaceHandler());
		return list;
	}
	

	@Override
	public List<NativePlace> getCities(String provinceCode) {
		String sql="select id,code,name from uim_np where length(code)=4 and left(code,2)=?";
		return JdbcTemplate.query(sql, new NativePlaceHandler(), provinceCode);
	}


	@Override
	public NativePlace getProvinceByCode(String provinceCode) {
		String sql="select id,code,name from uim_np where length(code)=2 and code=?";
		List<NativePlace> list=JdbcTemplate.query(sql, new NativePlaceHandler(),provinceCode);
		NativePlace nativePalce=null;
		if(null!=list&&list.size()==1) {
			nativePalce=list.get(0);
		}
		return nativePalce;
	}


	@Override
	public List<NativePlace> getCities() {
		String sql="select id,code,name from uim_np where length(code)=4";
		return JdbcTemplate.query(sql, new NativePlaceHandler());
	}

	@Override
	public List<NativePlace> getAllByPage() {
		String sql="select id,code,name from uim_np limit ?,?";
		//TODO:该代码没有实现。
		return null;
	}

	@Override
	public NativePlace getCityByCode(String code) {
		String sql="select id,code,name from uim_np where code=?";
		return JdbcTemplate.query(sql, new NativePlaceHandler(), code).get(0);
	}
}
