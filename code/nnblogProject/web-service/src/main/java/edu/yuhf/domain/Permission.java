package edu.yuhf.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
public class Permission {

	@NonNull
	private int id;
	@NonNull
	private int pid;
	@NonNull
	private String permissionName;
	@NonNull
	private String url;
	@NonNull
	private String remark;
	
	private boolean checked=false;
}
