package edu.yuhf.dao.convert;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.yuhf.dbutils.ResultSetHandler;
import lombok.extern.log4j.Log4j;

@Log4j
public class ListMapHandler implements ResultSetHandler<List<Map<String,Object>>> {

	@Override
	public List<Map<String, Object>> handler(ResultSet rs) {
		List<Map<String,Object>> list0=new ArrayList<Map<String,Object>>();
		try {
			while(rs.next()) {
				Map<String,Object> map=new HashMap<String,Object>();
				for(int i=0,len=rs.getMetaData().getColumnCount();i<len;i++) {
					map.put(rs.getMetaData().getColumnLabel(i+1), rs.getObject(i+1));
				}
				list0.add(map);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list0;
	}

}
