package edu.yuhf.dao.iface;

import java.util.List;

import edu.yuhf.domain.NativePlace;

public interface NativePlaceDao {

	public List<NativePlace> getProvinces();
	public NativePlace getProvinceByCode(String privinceCode);
	public List<NativePlace> getCities(String provinceCode);
	public List<NativePlace> getCities();
	public List<NativePlace> getAllByPage();
	public NativePlace getCityByCode(String code);
}
