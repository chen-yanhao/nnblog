package edu.yuhf.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Search {
    private String keyword;
    private int menuLevel;
    private int sex;
}
