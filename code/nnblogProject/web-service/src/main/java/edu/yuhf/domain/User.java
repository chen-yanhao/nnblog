package edu.yuhf.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

	private int id;
	private String userName;
	private String nickName;
	private String password;
	private String sex;
	private String email;
	private String phone;

	public User(String userName, String nickName, String password, String sex, String email, String phone) {
		super();
		this.userName = userName;
		this.nickName = nickName;
		this.password = password;
		this.sex = sex;
		this.email = email;
		this.phone = phone;
	}
}
