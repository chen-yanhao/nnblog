package edu.yuhf.dao.iface;

import java.util.List;

import edu.yuhf.domain.Page;
import edu.yuhf.domain.Role;

public interface RoleDao {

	public void queryRoles(Page<List<Role>> page);
	public List<Role> queryRoles();
	public int queryTotalRowNumber(Page<List<Role>> page);
	
	public int insertRole(Role role);
	public int deleteRoles(String ids);
	public Role queryRoleById(int id);
	public List<Role> queryRolesByUserId(int userId);
	public int updateRole(Role role);
}
