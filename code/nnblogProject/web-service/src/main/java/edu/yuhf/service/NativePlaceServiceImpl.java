package edu.yuhf.service;

import java.util.List;

import edu.yuhf.dao.iface.NativePlaceDao;
import edu.yuhf.dao.mysql.NativePlaceJdbcDaoImpl;
import edu.yuhf.domain.NativePlace;
import edu.yuhf.service.iface.NativePlaceService;

public class NativePlaceServiceImpl implements NativePlaceService {
	
	private NativePlaceDao nativePlaceDao=new NativePlaceJdbcDaoImpl();

	@Override
	public List<NativePlace> getProvince() {
		return nativePlaceDao.getProvinces();
	}

	@Override
	public List<NativePlace> getCities(String provinceCode) {
		return nativePlaceDao.getCities(provinceCode);
	}
	@Override
	public List<NativePlace> getCities() {
		return nativePlaceDao.getCities();
	}
}
