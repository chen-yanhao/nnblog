package edu.yuhf.dao.iface;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import edu.yuhf.domain.Page;
import edu.yuhf.domain.Permission;
import edu.yuhf.domain.User;

public interface UserDao {

	public List<User> showAllUser();
	public Optional<User> queryUserById(int id);
	public List<Map<String,Object>> queryAllUserAndUserDetail();
	public void queryAllUserAndUserDetailByPage(Page<List<Map<String,Object>>> page);
	public int queryTotalRowNumber();
	public int queryTotalRowNumber(String keyword);
	public Map<String,Object> queryUserAndUserDetailById(int id);
	public int queryUserForNameAndPwd(String userName,String password);
	public Optional<User> queryUser(String userName,String password);
	public int insertForUserId(User user);
	public int checkUserName(String userName);
	public int deleteUser(String ids);
	public int updateUser(User user);
	
}
