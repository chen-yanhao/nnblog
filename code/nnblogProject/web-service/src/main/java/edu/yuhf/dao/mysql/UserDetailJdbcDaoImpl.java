package edu.yuhf.dao.mysql;

import java.sql.SQLException;

import com.yuhf.dbutils.JdbcTemplate;

import edu.yuhf.dao.iface.UserDetailDao;
import edu.yuhf.domain.UserDetail;

public class UserDetailJdbcDaoImpl implements UserDetailDao {

	@Override
	public int insertUserDetail(UserDetail ud) {
		int rowNumber=0;
		String sql="insert into uim_user_detail(np_code,hobby_code,user_id,birthday) values(?,?,?,str_to_date(?,'%Y-%m-%d'))";
		rowNumber=JdbcTemplate.update(sql, ud.getNativePalceCode(),ud.getHobbyCode(),ud.getUserId(),ud.getBirthday());
		return rowNumber;
	}

	@Override
	public int deleteUserDetail(String ids) {
		int rowNumber=0;
		String sql="delete from uim_user_detail where id in("+ids+")";
		rowNumber=JdbcTemplate.update(sql);
		return rowNumber;
	}

	@Override
	public int getIdByUserId(int userId) {
		String sql="select id from uim_user_detail where user_id=?";
		Integer intId=JdbcTemplate.query(sql, rs->{
			Integer intId0=new Integer(0);
			try {
				if(rs.next()) {
					intId0=rs.getInt(1);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return intId0;
		},userId);
		return intId.intValue();
	}

	@Override
	public int updateUserDetail(UserDetail ud) {
		int rowNumber=0;
		String sql="update uim_user_detail set np_code=?,hobby_code=?,birthday=?  where user_id=?";
		rowNumber=JdbcTemplate.update(sql, ud.getNativePalceCode(),ud.getHobbyCode(),ud.getBirthday(),ud.getUserId());
		return rowNumber;
	}

}
