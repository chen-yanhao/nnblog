package edu.yuhf.service.iface;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import edu.yuhf.domain.Page;
import edu.yuhf.domain.User;
import edu.yuhf.domain.UserDetail;

public interface UserService {

	public Optional<User> checkUserNameAndPassword(String userName,String password) ;
	public boolean checkUserName(String userName);
	public boolean insertUserInfo(User user,UserDetail userDetail);
	
	public List<Map<String,Object>> queryAllUserAndUserDetail();
	public void queryAllUserAndUserDetail(Page<List<Map<String,Object>>> page);
	public Optional<Map<String,Object>> queryUserAndUserDetailById(int id);
	
	public boolean deleteUser(String ids);
	
	public Optional<User> getUser(int id);
	
	public boolean updateUserAndUserDetail(User user,UserDetail ud);
	
}
