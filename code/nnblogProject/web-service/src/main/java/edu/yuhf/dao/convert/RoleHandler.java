package edu.yuhf.dao.convert;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.yuhf.dbutils.ResultSetHandler;
import com.yuhf.dbutils.exception.ConvertException;

import edu.yuhf.domain.Role;

public class RoleHandler implements ResultSetHandler<List<Role>>{

	private static Logger log=Logger.getLogger(RoleHandler.class);
	@Override
	public List<Role> handler(ResultSet rs) {
		List<Role> list0=new ArrayList<>();
		try {
			while(rs.next()) {
				Role role=new Role(rs.getInt(1),rs.getString(2),rs.getString(3));
				list0.add(role);
			}
		} catch (SQLException e) {
			log.error("convert error,error message is "+e.getMessage());
			throw new ConvertException("Role convert rs error.");
		}
		return list0;
	}

}
