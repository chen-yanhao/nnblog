package edu.yuhf.dao.convert;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.yuhf.dbutils.ResultSetHandler;
import com.yuhf.dbutils.exception.ConvertException;

import edu.yuhf.domain.NativePlace;

public class NativePlaceHandler implements ResultSetHandler<List<NativePlace>> {
	
	private static Logger log=Logger.getLogger(NativePlaceHandler.class);

	@Override
	public List<NativePlace> handler(ResultSet rs) {
		List<NativePlace> list=new ArrayList<>();
		try {
			while(rs.next()) {
				NativePlace np=new NativePlace(rs.getInt(1),rs.getString(2),rs.getString(3));
				list.add(np);
			}
		} catch (SQLException e) {
			log.error("natviePlace convert error!");
			throw new ConvertException("NativePlace convert error! error message is "+e.getMessage());
		}
		return list;
	}

}
