package edu.yuhf.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
public class Hobby {

	@NonNull
	private int id;
	@NonNull
	private int code;
	@NonNull
	private String name;
	private boolean checked;
	
}
