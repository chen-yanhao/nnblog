package edu.yuhf.dao.mysql;

import java.util.List;
import java.util.Objects;

import com.yuhf.dbutils.JdbcTemplate;
import com.yuhf.dbutils.ResultSetHandler;

import edu.yuhf.dao.convert.PermissionHandler;
import edu.yuhf.dao.iface.PermissionDao;
import edu.yuhf.domain.Page;
import edu.yuhf.domain.Permission;

public class PermissionJdbcDaoImpl implements PermissionDao{
	@Override
	public List<Permission> queryPermissionByUserId(int id){
		List<Permission> list=null;
		String sql="select distinct p.id,p.pid,p.permission_name as permissionName,p.url,p.remark from uim_user u inner join umm_user_role ur on u.id=ur.user_id \n" +
				"\t inner join umm_permission_role pr on ur.role_id=pr.role_id\n" +
				"\t inner join umm_permission p on pr.permission_id=p.id where u.id=?";
		list=JdbcTemplate.query(sql,new PermissionHandler(),id);
		return list;
	}

	@Override
	public void queryPermissions(Page<List<Permission>> page) {
		String sql="select * from umm_permission where 1=1 ";
		if(!"".equals(page.getSearch().getKeyword())) {
			sql+="and (permission_name like ? or remark like ?) ";
		}
		if(page.getSearch().getMenuLevel()!=-1){
			if(page.getSearch().getMenuLevel()==0){
				sql+=" and pid=?";
			}else{
				sql+=" and pid>=?";
			}
		}
		sql+=" limit ?,?";
		List<Permission> list=null;
		ResultSetHandler<List<Permission>> rsh=new PermissionHandler();
		if(page.getSearch().getMenuLevel()==-1&& Objects.equals(page.getSearch().getKeyword(),"")){
			list=JdbcTemplate.query(sql, rsh,(page.getCurrentPage()-1)*page.getPageNumber(),page.getPageNumber());
		}
		if("".equals(page.getSearch().getKeyword())&&page.getSearch().getMenuLevel()!=-1) {
			list=JdbcTemplate.query(sql, rsh,page.getSearch().getMenuLevel(),(page.getCurrentPage()-1)*page.getPageNumber(),page.getPageNumber());
		}
		if(!"".equals(page.getSearch().getKeyword())&&page.getSearch().getMenuLevel()==-1) {
			list=JdbcTemplate.query(sql, rsh,"%"+page.getSearch().getKeyword()+"%","%"+page.getSearch().getKeyword()+"%",(page.getCurrentPage()-1)*page.getPageNumber(),page.getPageNumber());
		}
		if(!"".equals(page.getSearch().getKeyword())&&page.getSearch().getMenuLevel()!=-1) {
			list=JdbcTemplate.query(sql, rsh,"%"+page.getSearch().getKeyword()+"%","%"+page.getSearch().getKeyword()+"%",page.getSearch().getMenuLevel(),(page.getCurrentPage()-1)*page.getPageNumber(),page.getPageNumber());
		}
		page.setList(list);
	}

	@Override
	public int queryTotalRowNumber(Page<List<Permission>> page) {
		int totalRowNumber=0;
		String sql="select count(*) from umm_permission where 1=1 ";
		if(!"".equals(page.getSearch().getKeyword())) {
			sql+="and (permission_name like ? or remark like ?) ";
		}
		if(page.getSearch().getMenuLevel()!=-1){
			if(page.getSearch().getMenuLevel()==0){
				sql+=" and pid=?";
			}else{
				sql+=" and pid>=?";
			}
		}

		if(page.getSearch().getMenuLevel()==-1&& Objects.equals(page.getSearch().getKeyword(),"")){
			totalRowNumber=JdbcTemplate.queryForCount(sql);
		}
		if("".equals(page.getSearch().getKeyword())&&page.getSearch().getMenuLevel()!=-1) {
			totalRowNumber=JdbcTemplate.queryForCount(sql, page.getSearch().getMenuLevel());
		}
		if(!"".equals(page.getSearch().getKeyword())&&page.getSearch().getMenuLevel()==-1) {
			totalRowNumber=JdbcTemplate.queryForCount(sql, "%"+page.getSearch().getKeyword()+"%","%"+page.getSearch().getKeyword()+"%");
		}
		if(!"".equals(page.getSearch().getKeyword())&&page.getSearch().getMenuLevel()!=-1) {
			totalRowNumber=JdbcTemplate.queryForCount(sql, "%"+page.getSearch().getKeyword()+"%","%"+page.getSearch().getKeyword()+"%",page.getSearch().getMenuLevel());
		}
		return totalRowNumber;
	}

	@Override
	public int insertPermission(Permission permission) {
		int rowNumber=0;
		String sql="insert into umm_permission(pid,permission_name,url,remark) values(?,?,?,?)";
		rowNumber=JdbcTemplate.update(sql, permission.getPid(),permission.getPermissionName(),permission.getUrl(),permission.getRemark());
		return rowNumber;
	}

	@Override
	public int deletePermissions(String ids) {
		int rowNumber=0;
		String sql="delete from umm_permission where id in("+ids+")";
		rowNumber=JdbcTemplate.update(sql);
		return rowNumber;
	}

	@Override
	public Permission queryPermissionById(int id) {
		Permission permission=null;
		String sql="select id,pid,permission_name as permissionName,url,remark from umm_permission where id=?";
		ResultSetHandler<List<Permission>> rsh=new PermissionHandler();
		List<Permission> list=JdbcTemplate.query(sql, rsh, id);
		if(null!=list&&list.size()!=0){
			permission=list.get(0);
		}
		return permission;
	}

	@Override
	public int updatePermission(Permission permission) {
		int rowNumber=0;
		String sql="update umm_permission set pid=?,permission_name=?,url=?,remark=? where id=?";
		rowNumber=JdbcTemplate.update(sql, permission.getPid(),permission.getPermissionName(),permission.getUrl(),permission.getRemark(),permission.getId());
		return rowNumber;
	}

	@Override
	public List<Permission> queryParentPermissions() {
		List<Permission> list=null;
		String sql="select * from umm_permission where pid=0";
		ResultSetHandler<List<Permission>> rsh=new PermissionHandler();
		list=JdbcTemplate.query(sql, rsh);
		return list;
	}

	@Override
	public List<Permission> queryAllPermission() {
		List<Permission> list=null;
		String sql="select * from umm_permission";
		ResultSetHandler<List<Permission>> rsh=new PermissionHandler();
		list=JdbcTemplate.query(sql, rsh);
		return list;
	}

	@Override
	public List<Permission> querySubPermission() {
		List<Permission> list=null;
		String sql="select * from umm_permission where pid!=0";
		ResultSetHandler<List<Permission>> rsh=new PermissionHandler();
		list=JdbcTemplate.query(sql, rsh);
		return list;
	}

}
