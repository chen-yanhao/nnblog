package edu.yuhf.dao.iface;

public interface UserRoleDao {

	public int deleteData(int userId);
	public int insertData(int userId, String[] roleIds);
}
