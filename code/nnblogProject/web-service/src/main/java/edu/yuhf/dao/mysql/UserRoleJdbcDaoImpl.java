package edu.yuhf.dao.mysql;

import com.yuhf.dbutils.JdbcTemplate;

import edu.yuhf.dao.iface.UserRoleDao;

public class UserRoleJdbcDaoImpl implements UserRoleDao{

	@Override
	public int deleteData(int userId) {
		int rowNumber=0;
		String sql="delete from umm_user_role where user_id=?";
		rowNumber=JdbcTemplate.update(sql, userId);
		return rowNumber;
	}

	@Override
	public int insertData(int userId, String[] roleIds) {
		int rowNumber=0;
		int count=0;
		StringBuilder sb=new StringBuilder("insert into umm_user_role(user_id,role_id) values");
		if(null!=roleIds&&roleIds.length!=0) {
			for(String roleId:roleIds) {
				if(count==0) {
					sb.append("(").append(userId).append(",").append(roleId).append(")");					
				}else {
					sb.append(",(").append(userId).append(",").append(roleId).append(")");	
				}
				count++;
			}		
			rowNumber=JdbcTemplate.update(sb.toString());
		}
		return rowNumber;
	}

}
