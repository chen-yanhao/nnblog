package edu.yuhf.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import edu.yuhf.dao.iface.PermissionDao;
import edu.yuhf.dao.iface.PermissionRoleDao;
import edu.yuhf.dao.mysql.PermissionJdbcDaoImpl;
import edu.yuhf.dao.mysql.PermissionRoleJdbcDaoImpl;
import edu.yuhf.domain.Page;
import edu.yuhf.domain.Permission;
import edu.yuhf.service.iface.PermissionService;

public class PermissionServiceImpl implements PermissionService{

	private PermissionDao permissionDao=new PermissionJdbcDaoImpl();
	private PermissionRoleDao permissionRoleDao=new PermissionRoleJdbcDaoImpl();
	
	@Override
	public void getPermission(Page<List<Permission>> page) {
		permissionDao.queryPermissions(page);
		 int totalRowNumber=permissionDao.queryTotalRowNumber(page);
		 int totalPageNumber=0==totalRowNumber%page.getPageNumber()?totalRowNumber/page.getPageNumber():totalRowNumber/page.getPageNumber()+1;
		 page.setTotalRowNumber(totalRowNumber);
		 page.setTotalPageNumber(totalPageNumber);
		
	}

	@Override
	public boolean addPermission(Permission permission) {
		boolean flag=false;
		if(1==permissionDao.insertPermission(permission)) {
			flag=true;
		}
		return flag;
	}

	@Override
	public boolean removePermission(String ids) {
		boolean flag=false;
		if(permissionDao.deletePermissions(ids)!=0) {
			flag=true;
		}
		return flag;
	}

	@Override
	public Permission getPermissionById(int id) {
		return permissionDao.queryPermissionById(id);
	}

	@Override
	public boolean setPermission(Permission permission) {
		boolean flag=false;
		if(permissionDao.updatePermission(permission)==1) {
			flag=true;
		}
		return flag;
	}

	@Override
	public List<Permission> getParentPermission() {
		return permissionDao.queryParentPermissions();
	}

	@Override
	public List<Permission> getPermissionsByRoleId(int roleId) {
		List<Permission> rolePermissions=permissionRoleDao.queryPermissionsByRoleId(roleId);
		List<Permission> list=permissionDao.queryAllPermission();
		//在所有的权限中，给当前角色所包含的权限加上checked=true。
		list.forEach(element->{
			rolePermissions.forEach(permission->{
				if(element.getId()==permission.getId()) {
					element.setChecked(true);
				}
			});
		});
		return list;
	}

	@Override
	public List<Permission> getPermissionByUserId(int id) {
		List<Permission> list=permissionDao.queryPermissionByUserId(id);
		List<Permission> parentList=permissionDao.queryParentPermissions();
		List<Permission> otherList=new ArrayList();
		parentList.forEach(parentPermission->{
			if(!list.contains(parentPermission)){
				/*
				for(Permission p:list){
					if(p.getPid()==parentPermission.getId()){
						list.add(parentPermission);
						break;
					}
				}
				 */
				/*
				list.stream().filter(permission -> permission.getPid()==parentPermission.getId()).forEach(permission -> {
					if(!otherList.contains(parentPermission)) {
						otherList.add(parentPermission);
					}
				});
				 */
				Optional<Permission> optional=list.stream().filter(permission -> permission.getPid()==parentPermission.getId()).findAny();
				optional.ifPresent(permission->{
					if(!list.contains(parentPermission)) {
						list.add(parentPermission);
					}
				});
			}
		});
		//otherList.stream().distinct();
		//list.addAll(otherList);
		return list;
	}

}
