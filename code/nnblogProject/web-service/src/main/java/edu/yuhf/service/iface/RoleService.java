package edu.yuhf.service.iface;

import java.util.List;
import java.util.Optional;

import edu.yuhf.domain.Page;
import edu.yuhf.domain.Role;

public interface RoleService {

	public void getRoles(Page<List<Role>> page);
	public boolean addRole(Role role);
	public boolean removeRole(String ids);
	public Optional<Role> getRoleById(int id);
	public boolean setRole(Role role);
	public List<Role> getRolesByUserId(int userId);
}
