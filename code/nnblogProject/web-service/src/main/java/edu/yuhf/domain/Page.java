package edu.yuhf.domain;

import edu.yuhf.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class Page<T>{
	
	@NonNull
	private int currentPage;
	private int pageNumber=Constants.PAGE_NUMBER;
	private int totalRowNumber;
	private int totalPageNumber;
	
	private String keyword="";
	private Search search;
	
	private T list;

}
