package edu.yuhf.service;

import java.sql.SQLException;

import com.yuhf.dbutils.TransactionManager;
import com.yuhf.dbutils.exception.DaoException;

import edu.yuhf.dao.iface.PermissionRoleDao;
import edu.yuhf.dao.mysql.PermissionRoleJdbcDaoImpl;
import edu.yuhf.service.iface.PermissionRoleService;
import lombok.extern.log4j.Log4j;

@Log4j
public class PermissionRoleServiceImpl implements PermissionRoleService{
	
	private PermissionRoleDao permissionRoleDao=new PermissionRoleJdbcDaoImpl();

	@Override
	public boolean setPermissionRole(int roleId, String[] permissionIds) {
		boolean flag=false;
		TransactionManager tm=new TransactionManager();
		try {
			tm.beginTransaction();
			permissionRoleDao.deleteData(roleId);
			if(permissionRoleDao.insertData(roleId, permissionIds)>0) {
				flag=true;
			}			
			tm.commit();
		} catch (SQLException e) {
			tm.rollback();
			log.error("setPermissionRole error,error message is "+e.getMessage());
			throw new DaoException();
		}
		return flag;
	}

}
