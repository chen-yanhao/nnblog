package edu.yuhf.dao.iface;

import java.util.List;

import edu.yuhf.domain.Permission;

public interface PermissionRoleDao {

	public int deleteData(int roleId);
	public int insertData(int roleId, String[] permissionIds);
	public List<Permission> queryPermissionsByRoleId(int roleId);
}
