package edu.yuhf.dao.iface;

import java.util.List;

import edu.yuhf.domain.Page;
import edu.yuhf.domain.Permission;

public interface PermissionDao {
	public void queryPermissions(Page<List<Permission>> page);
	public int queryTotalRowNumber(Page<List<Permission>> page);
	
	public int insertPermission(Permission permission);
	public int deletePermissions(String ids);
	public Permission queryPermissionById(int id);
	public int updatePermission(Permission permission);
	
	public List<Permission> queryParentPermissions();
	public List<Permission> queryAllPermission();
	public List<Permission> querySubPermission();

	public List<Permission> queryPermissionByUserId(int id);
}
