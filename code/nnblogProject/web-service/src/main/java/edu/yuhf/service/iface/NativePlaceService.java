package edu.yuhf.service.iface;

import java.util.List;

import edu.yuhf.domain.NativePlace;

public interface NativePlaceService {

	public List<NativePlace> getProvince();
	public List<NativePlace> getCities(String provinceCode);
	public List<NativePlace> getCities();
}
