package edu.yuhf.dao.convert;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.yuhf.dbutils.ResultSetHandler;
import com.yuhf.dbutils.exception.ConvertException;

import edu.yuhf.domain.Permission;

public class PermissionHandler implements ResultSetHandler<List<Permission>>{

	private static Logger log=Logger.getLogger(RoleHandler.class);
	@Override
	public List<Permission> handler(ResultSet rs) {
		List<Permission> list0=new ArrayList<>();
		try {
			while(rs.next()) {
				Permission permission=new Permission(rs.getInt(1),rs.getInt(2),rs.getString(3),rs.getString(4),rs.getString(5));
				list0.add(permission);
			}
		} catch (SQLException e) {
			log.error("convert error,error message is "+e.getMessage());
			throw new ConvertException("permission convert rs error.");
		}
		return list0;
	}

}
