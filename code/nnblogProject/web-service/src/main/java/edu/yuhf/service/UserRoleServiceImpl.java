package edu.yuhf.service;

import java.sql.SQLException;

import com.yuhf.dbutils.TransactionManager;
import com.yuhf.dbutils.exception.DaoException;

import edu.yuhf.dao.iface.UserRoleDao;
import edu.yuhf.dao.mysql.UserRoleJdbcDaoImpl;
import edu.yuhf.service.iface.UserRoleService;
import lombok.extern.log4j.Log4j;

@Log4j
public class UserRoleServiceImpl implements UserRoleService{

	private UserRoleDao userRoleDao=new UserRoleJdbcDaoImpl();
	
	@Override
	public boolean setUserRole(int userId, String[] roleIds) {
		boolean flag=false;
		TransactionManager tm=new TransactionManager();
		try {
			tm.beginTransaction();
			userRoleDao.deleteData(userId);
			if(userRoleDao.insertData(userId, roleIds)>0) {
				//TODO:此处逻辑有漏洞，当取值为0时，可能不是更新失败。
				flag=true;
			}			
			tm.commit();
		} catch (SQLException e) {
			tm.rollback();
			log.error("setUserRole error,error message is "+e.getMessage());
			throw new DaoException();
		}
		return flag;
	}

}
