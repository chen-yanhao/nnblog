package edu.yuhf.service.iface;

import java.util.List;

import edu.yuhf.domain.Page;
import edu.yuhf.domain.Permission;

public interface PermissionService {
	public void getPermission(Page<List<Permission>> page);
	
	public boolean addPermission(Permission permission);
	public boolean removePermission(String ids);
	public Permission getPermissionById(int id);
	public boolean setPermission(Permission permission);
	
	public List<Permission> getParentPermission();
	public List<Permission> getPermissionsByRoleId(int roleId);

	public List<Permission> getPermissionByUserId(int id);
}
