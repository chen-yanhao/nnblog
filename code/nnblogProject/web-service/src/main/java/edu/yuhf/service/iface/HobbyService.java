package edu.yuhf.service.iface;

import java.util.List;

import edu.yuhf.domain.Hobby;

public interface HobbyService {

	public List<Hobby> showHobbies();
}
