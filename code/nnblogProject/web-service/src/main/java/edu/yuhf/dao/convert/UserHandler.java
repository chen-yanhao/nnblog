package edu.yuhf.dao.convert;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.yuhf.dbutils.ResultSetHandler;
import com.yuhf.dbutils.exception.ConvertException;

import edu.yuhf.domain.User;

public class UserHandler implements ResultSetHandler<List<User>> {

	private static Logger log=Logger.getLogger(UserHandler.class);
	
	@Override
	public List<User> handler(ResultSet rs) {
		List<User> list0=new ArrayList<User>();
		try {
			while(rs.next()) {
				User user=new User(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7));
				list0.add(user);
			}
		} catch (SQLException e) {
			log.error(e.getMessage());
			throw new ConvertException("NativePlace convert error! error message is "+e.getMessage());
		}
		return list0;
	}

}
