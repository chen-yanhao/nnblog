package edu.yuhf.dao.iface;

import edu.yuhf.domain.UserDetail;

public interface UserDetailDao {

	public int insertUserDetail(UserDetail ud);
	public int deleteUserDetail(String id);
	public int getIdByUserId(int userId);
	public int updateUserDetail(UserDetail ud);
}
