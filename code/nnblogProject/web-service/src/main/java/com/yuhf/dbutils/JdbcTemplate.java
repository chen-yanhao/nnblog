package com.yuhf.dbutils;

import java.sql.Connection;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.stream.IntStream;

import lombok.extern.log4j.Log4j;
import org.apache.log4j.Logger;

import com.yuhf.dbutils.exception.DaoException;

@Log4j
public class JdbcTemplate {

	public static int updateForKey(String sql,Object...parameters) {
		int id=0;
		Connection connection=DBConnection.getConnection();
		try{
			PreparedStatement psmt=connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			if(null!=parameters && parameters.length!=0 && psmt.getParameterMetaData().getParameterCount()==parameters.length) {
				IntStream.range(0,parameters.length).forEach(index->{
					try {
						psmt.setObject(index+1,parameters[index]);
					} catch (SQLException e) {
						log.error("update parameters error,error message is "+e.getMessage());
					}
				});
			}
			int rowNumber=psmt.executeUpdate();
			if(rowNumber==1) {
				ResultSet rs=psmt.getGeneratedKeys();
				if(rs.next()) {
					id=rs.getInt(1);
				}
			}
		} catch (SQLException e) {
			log.error("update for key operation error,error message is "+e.getMessage());
			throw new DaoException("update for key operation error,error message is "+e.getMessage());
		} finally {
			try {
				if(connection.getAutoCommit()) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return id;
	}
	
	public static int update(String sql,Object...parameters) {
		int rowNumber=0;
		Connection connection=DBConnection.getConnection();
		try{
			PreparedStatement psmt=connection.prepareStatement(sql);
			if(null!=parameters&&parameters.length!=0&&parameters.length==psmt.getParameterMetaData().getParameterCount()) {
				for(int i=0,len=parameters.length;i<len;i++) {
					psmt.setObject(i+1, parameters[i]);
				}
			}
			rowNumber=psmt.executeUpdate();
		} catch (SQLException e) {
			log.error("update operation error,error message is "+e.getMessage());
			throw new DaoException("update operation error,error message is "+e.getMessage());
		}finally {
			try {
				if(connection.getAutoCommit()) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
		return rowNumber;
	}
	
	public static int queryForCount(String sql,Object...parameters) {
		int rowNumber=0;
		Connection connection=DBConnection.getConnection();
		try{
			PreparedStatement psmt=connection.prepareStatement(sql);
			if(null!=parameters&&parameters.length!=0&&parameters.length==psmt.getParameterMetaData().getParameterCount()) {
				for(int i=0,len=parameters.length;i<len;i++) {
					psmt.setObject(i+1, parameters[i]);
				}
			}
			ResultSet rs=psmt.executeQuery();
			if(rs.next()) {
				rowNumber=rs.getInt(1);
			}
		} catch (SQLException e) {
			log.error("queryForCount error,error message is "+e.getMessage());
			throw new DaoException("queryForCount error,error message is "+e.getMessage());
		}finally {
			try {
				if(connection.getAutoCommit()) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rowNumber;
	}
	
	public static <T> T query(String sql,ResultSetHandler<T> rsh,Object...parameters) {
		T t=null;
		Connection connection=DBConnection.getConnection();
		try{
			PreparedStatement psmt=connection.prepareStatement(sql);
			ParameterMetaData pmd=psmt.getParameterMetaData();
			if(null!=parameters&&parameters.length!=0&pmd.getParameterCount()==parameters.length) {
				for(int i=0,len=parameters.length;i<len;i++) {
					psmt.setObject(i+1, parameters[i]);
				}
			}
			ResultSet rs=psmt.executeQuery();
			t=rsh.handler(rs);		
		} catch (SQLException e) {
			log.error("query operation error,error message is "+e.getMessage());
			throw new DaoException("query operation error,error message is "+e.getMessage());
		}finally {
			try {
				if(connection.getAutoCommit()) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return t;
	}
}
