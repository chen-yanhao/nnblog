package com.yuhf.dbutils;

import java.sql.Connection;
import java.sql.SQLException;

import lombok.extern.log4j.Log4j;
import org.apache.log4j.Logger;

@Log4j
public class TransactionManager {

	public void beginTransaction() throws SQLException {
		Connection connection=DBConnection.getConnection();
		connection.setAutoCommit(false);

	}
	
	public void commit(){
		try(Connection connection=DBConnection.getConnection()){
			connection.commit();
			connection.setAutoCommit(true);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void rollback() {
		try(Connection connection=DBConnection.getConnection()){
			connection.rollback();
			connection.setAutoCommit(true);
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}
	
}
