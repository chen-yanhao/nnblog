package com.yuhf.dbutils.exception;

public class ConvertException extends RuntimeException {

	private static final long serialVersionUID = 627206011793987719L;
	
	public ConvertException() {}
	
	public ConvertException(String message) {
		super(message);
	}

}
