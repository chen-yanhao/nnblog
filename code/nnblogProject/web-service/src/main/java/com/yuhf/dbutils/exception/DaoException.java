package com.yuhf.dbutils.exception;

public class DaoException extends RuntimeException {

	private static final long serialVersionUID = 3981733822207125965L;
	
	public DaoException(){}
	
	public DaoException(String message) {
		super(message);
	}
}
