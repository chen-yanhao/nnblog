package com.yuhf.dbutils;

import java.sql.ResultSet;

@FunctionalInterface
public interface ResultSetHandler<T> {
	T handler(ResultSet rs);
}
