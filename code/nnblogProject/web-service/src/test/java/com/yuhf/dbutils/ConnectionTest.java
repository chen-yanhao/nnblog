package com.yuhf.dbutils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.junit.Test;

public class ConnectionTest {

	private static Logger log=Logger.getLogger(ConnectionTest.class);
	
	
	@Test
	public void dbConnectionTest() {
		Connection connection= DBConnection.getConnection();
		log.info("connection success");
	}
	
	@Test
	public void connectionTest() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		String sql="select * from uim_user";
		String connString="jdbc:mysql://192.168.10.213:3306/nnblog?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=GMT%2B8&allowMultiQueries=true&useAffectedRows=true&allowPublicKeyRetrieval=true";
		try {
			Connection connection=DriverManager.getConnection(connString,"nnblog","nnblog");
			PreparedStatement psmt=connection.prepareStatement(sql);
			ResultSet rs=psmt.executeQuery();
			while(rs.next()) {
				log.info(rs.getString("user_name")+","+rs.getString("nick_name"));
			}
		} catch (SQLException e) {
			log.error(e.getMessage());
		}
	}
}
