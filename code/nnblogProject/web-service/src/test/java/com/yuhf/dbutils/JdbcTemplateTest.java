package com.yuhf.dbutils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Test;

public class JdbcTemplateTest {
	
	private static Logger log=Logger.getLogger(JdbcTemplateTest.class);
	

	
	@Test
	public void testQuery() {
		String sql="select u.id,u.userName,u.nickName,u.password,u.sex,u.email,u.phone,np.name nativePlace_code,ud.hobby_code,ud.birthday "
				+ "from users u left outer join userDetail ud on u.id=ud.userId left outer join nativeplaces np on ud.nativePlace_code=np.code;";
		List<Map<String,Object>> list=JdbcTemplate.query(sql,(rs)->{
			List<Map<String,Object>> list0=new ArrayList<>();
			try {
				while(rs.next()) {
					Map<String,Object> map=new HashMap<>();
					for(int i=0,len=rs.getMetaData().getColumnCount();i<len;i++) {
						map.put(rs.getMetaData().getColumnName(i+1), rs.getObject(i+1));
					}
					list0.add(map);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return list0;
		});
		
		list.forEach((map)->{
			log.info(map.get("userName")+","+map.get("nickName")+","+map.get("email")+","+map.get("hobby_code"));
		});
	}
	
	@Test
	public void testQuery1() {
		String sql="select u.id,u.userName,u.nickName,u.password,u.sex,u.email,u.phone,np.name nativePlace_code,ud.hobby_code,ud.birthday "
				+ "from users u left outer join userDetail ud on u.id=ud.userId left outer join nativeplaces np on ud.nativePlace_code=np.code;";
		List<Map<String,Object>> list=JdbcTemplate.query(sql,new ResultSetHandler<List<Map<String,Object>>>(){
			@Override
			public List<Map<String,Object>> handler(ResultSet rs) {
				List<Map<String,Object>> list0=new ArrayList<>();
				try {
					while(rs.next()) {
						Map<String,Object> map=new HashMap<>();
						for(int i=0,len=rs.getMetaData().getColumnCount();i<len;i++) {
							map.put(rs.getMetaData().getColumnName(i+1), rs.getObject(i+1));
						}
						list0.add(map);
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				return list0;
			}
		});

		list.forEach((map)->{
			log.info(map.get("userName")+","+map.get("nickName")+","+map.get("email")+","+map.get("hobby_code"));
		});
	}
	
	@Test
	public void testQuery2() {
		String sql="select u.id,u.userName,u.nickName,u.password,u.sex,u.email,u.phone,np.name nativePlace_code,ud.hobby_code,ud.birthday "
				+ "from users u left outer join userDetail ud on u.id=ud.userId left outer join nativeplaces np on ud.nativePlace_code=np.code;";
		List<Map<String,Object>> list=JdbcTemplate.query(sql,new ResultSetHandlerList());
		
		list.forEach((map)->{
			log.info(map.get("userName")+","+map.get("nickName")+","+map.get("email")+","+map.get("hobby_code"));
		});
	}
	class ResultSetHandlerList implements ResultSetHandler<List<Map<String,Object>>>{
		@Override
		public List<Map<String, Object>> handler(ResultSet rs) {
			List<Map<String,Object>> list0=new ArrayList<>();
			try {
				while(rs.next()) {
					Map<String,Object> map=new HashMap<>();
					for(int i=0,len=rs.getMetaData().getColumnCount();i<len;i++) {
						map.put(rs.getMetaData().getColumnName(i+1), rs.getObject(i+1));
					}
					list0.add(map);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return list0;
		}
		
	}
}
