package edu.yuhf.dao.mysql;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import lombok.extern.log4j.Log4j;
import org.apache.log4j.Logger;
import org.junit.Test;

import edu.yuhf.dao.iface.UserDao;
import edu.yuhf.domain.Page;
import edu.yuhf.domain.User;

@Log4j
public class UserJdbcDaoImplTest {

	private UserDao userDao=new UserJdbcDaoImpl();
	
	@Test
	public void testQueryAllUserAndUserDetailByPage() {
		Page<List<Map<String,Object>>> page=new Page<>(1);
		userDao.queryAllUserAndUserDetailByPage(page);
		page.getList().forEach(log::info);
	}
	
	@Test
	public void testQueryUserAndUserDetailById() {
		Map<String,Object> map=userDao.queryUserAndUserDetailById(1);
		log.info(map.toString());
	}

	public void testDeleteUser() {
		int rowNumber=userDao.deleteUser("10");
		log.info("row number is:"+rowNumber);
	}
	@Test
	public void testShowAllUserAndUserDetail() {
		List<Map<String,Object>> list=userDao.queryAllUserAndUserDetail();
		log.info(list.toString());
		list.forEach((map)->{
			log.info(map.get("userName")+","+map.get("nickName")+","+map.get("nativeplace")+","+map.get("hobby"));
		});
	}
	@Test
	public void testShowAllUser() {
		List<User> list=userDao.showAllUser();
		list.forEach((user)->{
			log.info(user.getUserName());
		});
	}
	
	@Test
	public void testQueryUserForNameAndPwd() {
		int number=userDao.queryUserForNameAndPwd("admin", "admin1");
		log.info("number is "+number);
	}
	
	@Test
	public void testUpdateForUserId() {
		User user=new User("test02","熊大","123456","1","xd@qq.com","132874544");
		int id=userDao.insertForUserId(user);
		log.info("return id is:"+id);
	}
	
	@Test
	public void testCheckUserName() {
		int rowNumber=userDao.checkUserName("admin1");
		log.info("rowNumber:"+rowNumber);
	}

	@Test
	public void testQueryUserById(){
		Optional<User> optional=userDao.queryUserById(3);
		optional.ifPresent((user)->log.debug(user.getUserName()));
	}
}
