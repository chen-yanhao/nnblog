package edu.yuhf.dao.mysql;

import org.junit.Test;

import edu.yuhf.dao.iface.PermissionRoleDao;
import lombok.extern.log4j.Log4j;

@Log4j
public class PermissionRoleJdbcDaoImplTest {

	private PermissionRoleDao permissionRoleDao=new PermissionRoleJdbcDaoImpl();
	
	@Test
	public void testInsertData() {
		permissionRoleDao.insertData(1, new String[] {"1","2","3"});
	}
	
	@Test
	public void deleteDataTest() {
		int rowNumber=permissionRoleDao.deleteData(2);
		log.debug(rowNumber);
	}
}
