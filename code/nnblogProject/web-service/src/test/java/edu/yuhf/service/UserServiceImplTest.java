package edu.yuhf.service;

import java.util.Map;
import java.util.Optional;

import org.junit.Test;

import edu.yuhf.service.iface.UserService;

public class UserServiceImplTest {
	
	private UserService userService = new UserServiceImpl();

	@Test
	public void testCheckUserName() {
		System.out.println(userService.checkUserName("admin1"));
	}
	
	@Test
	public void testQueryAllUserAndUserDetail() {
		userService.queryAllUserAndUserDetail();
	}
	
	@Test
	public void testQueryUserAndUserDetailById() {
		Optional<Map<String,Object>> optional=userService.queryUserAndUserDetailById(1);
		optional.ifPresent(test->{
			System.out.println(test);
		});
	}
}
