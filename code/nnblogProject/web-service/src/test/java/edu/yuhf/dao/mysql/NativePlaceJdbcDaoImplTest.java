package edu.yuhf.dao.mysql;

import java.util.List;

import lombok.extern.log4j.Log4j;
import org.junit.Test;

import edu.yuhf.dao.iface.NativePlaceDao;
import edu.yuhf.domain.NativePlace;

@Log4j
public class NativePlaceJdbcDaoImplTest {
	
	private NativePlaceDao nativePlaceDao=new NativePlaceJdbcDaoImpl();

	@Test
	public void testGetProvinces() {
		List<NativePlace> list=nativePlaceDao.getProvinces();
		list.forEach(System.out::println);
	}
	
	@Test
	public void testGetCities() {
		List<NativePlace> list=nativePlaceDao.getCities("37");
		list.forEach(System.out::println);		
	}
}
