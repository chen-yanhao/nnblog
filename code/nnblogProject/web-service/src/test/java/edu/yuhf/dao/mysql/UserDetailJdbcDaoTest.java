package edu.yuhf.dao.mysql;

import java.time.LocalDate;

import lombok.extern.log4j.Log4j;
import org.junit.Test;

import edu.yuhf.dao.iface.UserDetailDao;
import edu.yuhf.domain.UserDetail;

@Log4j
public class UserDetailJdbcDaoTest {

	private UserDetailDao userDetailDao=new UserDetailJdbcDaoImpl();
	
	
	@Test
	public void testUpdateUserDetail() {
		UserDetail ud=new UserDetail(0,"3701","1,2",9,LocalDate.parse("2018-04-09"));
		userDetailDao.updateUserDetail(ud);
	}
	@Test
	public void testDeleteUserDetail() {
		int rowNumber=userDetailDao.deleteUserDetail("13,14");
		System.out.println("row number is:"+rowNumber);
	}
	@Test
	public void testInsertUserDetail() {
		UserDetail ud=new UserDetail(0,"3702","2,3",6,LocalDate.of(2018,12,2));
		int rowNubmer=userDetailDao.insertUserDetail(ud);
		System.out.println(rowNubmer);
	}
	@Test
	public void testGetIdByUserId() {
		int rowNumber=userDetailDao.getIdByUserId(16);
		System.out.println("row number is:"+rowNumber);
	}
}
