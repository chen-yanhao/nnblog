package edu.yuhf.dao.mysql;

import java.util.List;

import lombok.extern.log4j.Log4j;
import org.junit.Test;

import edu.yuhf.dao.iface.HobbyDao;
import edu.yuhf.domain.Hobby;

@Log4j
public class HobbyJdbcDaoImplTest {
	
	private HobbyDao hobbyDao=new HobbyJdbcDaoImpl();

	@Test
	public void testGetHobbies() {
		List<Hobby> list=hobbyDao.getHobbies();
		list.forEach(System.out::println);
	}
}
