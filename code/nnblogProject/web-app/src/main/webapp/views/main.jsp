<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>主页面</title>
    <link rel="stylesheet" href="../css/main.css">
</head>
<script>
    function init() {
        document.querySelector(".level1Ol").addEventListener("click", (event) => {
            let object = event.target;
            if (object.classList.value == "level1Li" && object.nextElementSibling != null && object.nextElementSibling.tagName.toLowerCase() != "li") {
                object.nextElementSibling.toggleAttribute("hidden");
            }
        }, false);
    }
    window.addEventListener("load", init, false);
</script>

<body>
    <section class="mainPage">
        <nav class="top"></nav>
        <aside class="menu">
            <ol class="level1Ol">
            <c:forEach items="${sessionScope.permissions}" var="parentPermission">
                <c:if test="${parentPermission.pid==0}">
                    <li class="level1Li">${parentPermission.permissionName}</li>
                </c:if>
                <ol class="level2Ol">
                    <c:forEach items="${sessionScope.permissions}" var="permission">
                        <c:if test="${permission.pid==parentPermission.id}">
                            <li class="level2Li"><a href="${permission.url}" target="mainPage">${permission.permissionName}</a></li>
                        </c:if>
                    </c:forEach>
                </ol>
            </c:forEach>
        </ol>
        </aside>
        <article class="main">
            <iframe name="mainPage" src="views/welcome.html"></iframe>
        </article>
    </section>
</body>

</html>