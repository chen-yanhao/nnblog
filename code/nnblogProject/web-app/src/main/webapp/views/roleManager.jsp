<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/base.css">
    <link rel="stylesheet" href="/css/component.css">
    <link rel="stylesheet" href="/css/normal_table.css">
    <link rel="stylesheet" href="/css/roleManager.css">
    <script src="/js/tableOperation.js" type="module"></script>
    <script src="/js/axios.min.js"></script>
    <script src="/js/webUtils.js" type="module"></script>
    <script src="/js/roleManager.js" type="module"></script>
    <title>角色信息管理</title>
</head>

<body>
    <nav id="navigate">
        权限管理模块&nbsp;->&nbsp;角色信息管理页面&nbsp;<span id="operationMsg"></span>
    </nav>
    <section id="roleTable">
        <nav id="navSearch">
        	<form action="role.servlet?param=init" method="post">
	            <input type="search" id="keyword" name="keyword" class="normal-input" value="${requestScope.page.keyword}" style="height: 35px;">
	            <button type="submit" class="login-button">查询</button>
            </form>
        </nav>
        <article id="main">
            <table class="norTable">
                <thead>
                    <tr>
                        <th><input type="checkbox" id="all"></th>
                        <th>角色名</th>
                        <th>角色说明</th>
                    </tr>
                </thead>
                <tbody id="norTableTbody">
                	<c:forEach items="${requestScope.page.list}" var="row">
                    <tr>
                        <td style="text-align: center"><input type="checkbox" data-oid="${row.id}" class="cbxSelect"></td>
                        <td>${row.roleName}</td>
                        <td>${row.remark}</td>
                    </tr>
                    </c:forEach>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10">
                            <nav style="float: left;">
                                <button id="btnAdd" class="login-button">新增</button>
                                <button id="btnDelete" class="login-button">删除</button>
                                <button id="btnUpdate" class="login-button">修改</button>
                                <button id="btnPermission" class="login-button">修改权限</button>
                            </nav>
                            <nav id="pageGroup" style="float:right">
                           		当前第<span id="currentPage">${requestScope.page.currentPage}</span>页&nbsp;
                            	共<span id="totalPageNumber">${requestScope.page.totalPageNumber}</span>页&nbsp;
                            	每页${requestScope.page.pageNumber}行
                            	&nbsp;
                                <c:if test="${requestScope.page.currentPage==1}">首页</c:if>
                            	<c:if test="${requestScope.page.currentPage!=1}"><a href="#" id="first">首页</a></c:if>
                            	&nbsp;
                            	<c:if test="${requestScope.page.currentPage==1}">上一页</c:if>
                            	<c:if test="${requestScope.page.currentPage!=1}"><a href="#" id="previous">上一页</a></c:if>
                            	&nbsp;
                                <select id="jumpPage" class="normal-input" style="width:50px;height:30px;">
                                	<c:forEach begin="1" end="${requestScope.page.totalPageNumber }" var="i">
                                		<c:if test="${i==requestScope.page.currentPage}">
                                			<option selected  value="${i }">${i }</option>
                                		</c:if>
                                		<c:if test="${i!=requestScope.page.currentPage}">
                                			<option value="${i }">${i }</option>
                                		</c:if>
                                	</c:forEach>
                                </select>
								&nbsp;
								<c:if test="${requestScope.page.currentPage==requestScope.page.totalPageNumber}">下一页</c:if>
								<c:if test="${requestScope.page.currentPage!=requestScope.page.totalPageNumber}">
                                <a href="#" id="next">下一页</a>
                                </c:if>
								&nbsp;
								<c:if test="${requestScope.page.currentPage==requestScope.page.totalPageNumber}">尾页</c:if>
								<c:if test="${requestScope.page.currentPage!=requestScope.page.totalPageNumber}">
                                <a href="#" id="last">尾页</a>
                                </c:if>
                            </nav>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </article>
    </section>
    <section id="roleOperation" hidden>
    	<form id="addOrUpdateForm" action="" method="post">
	        <fieldset style="border:0px;">
	        	<input type="hidden" name="id" id="id">
	            <legend hidden>角色操作</legend>
	           	<span style="width:100px;text-align:right;">角色名：</span>
	           	<input type="text" name="roleName" id="roleName" class="normal-input"><br>
				<span style="width:100px;text-align:right;">角色备注</span>
				<input type="text" name="remark" id="remark" class="normal-input"><br>
	            <button type="button" id="return" class="login-button">返回</button>
	            <button type="button" id="operation" class="login-button">提交</button>
	        </fieldset>
        </form>
    </section>
    <section id="permissionOperation" hidden>
	    <nav id="role_title">角色名称：<span id="roleNameInsertPoint"></span></nav>
	    <form action="role.servlet?param=perUpdate" method="post">
	    <aside id="section_roleUpdate">
	    	<input type="hidden" name="roleId" id="roleId">
	        <table class="roleTable">
	            <tbody id="permissionInsertPoint">
	            <!--
	                <tr>
	                    <td class="module_level">
	                        <input type="checkbox" class="normalCheckbox permissionLevel1"><span class="normal_Text">用户权限管理模块</span>
	                    </td>
	                    <td class="page_level">
	                        <input type="checkbox" class="normalCheckbox"><span class="normal_Text">用户信息管理</span>&nbsp;
	                        <input type="checkbox" class="normalCheckbox"><span class="normal_Text">角色信息管理</span>&nbsp;
	                        <input type="checkbox" class="normalCheckbox"><span class="normal_Text">权限信息管理</span>
	                    </td>
	                </tr>
	                <tr>
	                    <td class="module_level">
	                        <input type="checkbox" class="normalCheckbox permissionLevel1"><span class="normal_Text">邮件管理模块</span>
	                    </td>
	                    <td class="page_level">
	                        <input type="checkbox" class="normalCheckbox"><span class="normal_Text">写邮件</span>&nbsp;
	                        <input type="checkbox" class="normalCheckbox"><span class="normal_Text">收邮件</span>&nbsp;
	                    </td>
	                </tr>
	                  -->
	            </tbody>
	        </table>
	    </aside>
	    <nav id="buttonGroup" style="text-align: center">
	        <button type="button" class="login-button" id="returnTable">返回</button>
	        <button type="submit" class="login-button">保存</button>
	    </nav>
	    </form>
    </section>
</body>
</html>