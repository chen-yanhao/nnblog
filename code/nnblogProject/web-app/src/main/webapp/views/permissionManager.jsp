<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/base.css">
    <link rel="stylesheet" href="/css/component.css">
    <link rel="stylesheet" href="/css/normal_table.css">
    <link rel="stylesheet" href="/css/permissionManager.css">
    <script src="/js/tableOperation.js" type="module"></script>
    <script src="/js/axios.min.js"></script>
    <script src="/js/webUtils.js" type="module"></script>
    <script src="/js/permissionManager.js" type="module"></script>
    <script src="/js/page.js" type="module"></script>
    <title>权限信息管理</title>
</head>

<body>
<nav id="navigate">
    权限管理模块&nbsp;->&nbsp;权限信息管理页面&nbsp;<span id="operationMsg"></span>
</nav>
<section id="permissionTable">
    <nav id="navSearch">
        <form action="permission.servlet?param=init" method="post">
            <select id="menuLevel" name="menuLevel" style="height: 39px;border-radius: 3px;">
                <option value="-1">菜单等级</option>
                <option value="0">一级菜单</option>
                <option value="1">二级菜单</option>
            </select>&nbsp;&nbsp;
            <input type="search" id="keyword" name="keyword" class="normal-input" value="${requestScope.page.search.keyword}" style="height: 40px;width:150px">
            &nbsp;&nbsp;
            <button type="submit" class="login-button">查询</button>
        </form>
    </nav>
    <article id="main">
        <table class="norTable">
            <thead>
            <tr>
                <th><input type="checkbox" id="all"></th>
                <th>父id</th>
                <th>权限名</th>
                <th>url</th>
                <th>权限说明</th>
            </tr>
            </thead>
            <tbody id="norTableTbody">
            <c:forEach items="${requestScope.page.list}" var="row">
                <tr>
                    <td style="text-align: center"><input type="checkbox" data-oid="${row.id}" class="cbxSelect"></td>
                    <td>${row.pid}</td>
                    <td>${row.permissionName}</td>
                    <td>${row.url}</td>
                    <td>${row.remark}</td>
                </tr>
            </c:forEach>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="10">
                    <nav style="float: left;">
                        <button id="btnAdd" class="login-button">新增</button>
                        <button id="btnDelete" class="login-button">删除</button>
                        <button id="btnUpdate" class="login-button">修改</button>
                    </nav>
                    <nav id="pageGroup" style="float:right">
                        当前第<span id="currentPage">${requestScope.page.currentPage}</span>页&nbsp;
                        共${requestScope.page.totalRowNumber}行数据
                        共<span id="totalPageNumber">${requestScope.page.totalPageNumber}</span>页&nbsp;
                        每页${requestScope.page.pageNumber}行
                        &nbsp;
                        <c:if test="${requestScope.page.currentPage==1}">首页</c:if>
                        <c:if test="${requestScope.page.currentPage!=1}"><a href="#" id="first">首页</a></c:if>
                        &nbsp;
                        <c:if test="${requestScope.page.currentPage==1}">上一页</c:if>
                        <c:if test="${requestScope.page.currentPage!=1}"><a href="#" id="previous">上一页</a></c:if>
                        &nbsp;
                        <select id="jumpPage" class="normal-input" style="width:50px;height:30px;">
                            <c:forEach begin="1" end="${requestScope.page.totalPageNumber }" var="i">
                                <c:if test="${i==requestScope.page.currentPage}">
                                    <option selected value="${i }">${i }</option>
                                </c:if>
                                <c:if test="${i!=requestScope.page.currentPage}">
                                    <option value="${i }">${i }</option>
                                </c:if>
                            </c:forEach>
                        </select>
                        &nbsp;
                        <c:if test="${requestScope.page.currentPage==requestScope.page.totalPageNumber}">下一页</c:if>
                        <c:if test="${requestScope.page.currentPage!=requestScope.page.totalPageNumber}">
                            <a href="#" id="next">下一页</a>
                        </c:if>
                        &nbsp;
                        <c:if test="${requestScope.page.currentPage==requestScope.page.totalPageNumber}">尾页</c:if>
                        <c:if test="${requestScope.page.currentPage!=requestScope.page.totalPageNumber}">
                            <a href="#" id="last">尾页</a>
                        </c:if>
                    </nav>
                </td>
            </tr>
            </tfoot>
        </table>
    </article>
</section>
<section id="permissionOperation" hidden>
    <form id="addOrUpdateForm" action="" method="post">
        <fieldset style="border:0px;">
            <input type="hidden" name="id" id="id">
            <legend hidden>权限操作</legend>
            <table>
                <tbody>
                <tr>
                    <td><span style="width:100px;text-align:right;">所属权限：</span></td>
                    <td><select name="pid" id="pid" class="normal-input" style="height:35px;">
                        <option value="0">一级权限</option>
                    </select>
                    </td>
                </tr>
                <tr>
                    <td><span style="width:100px;text-align:right;">权限名：</span></td>
                    <td><input type="text" name="permissionName" id="permissionName" class="normal-input"></td>
                </tr>
                <tr>
                    <td><span style="width:100px;text-align:right;">url：</span></td>
                    <td><input type="text" name="url" id="url" class="normal-input"></td>
                </tr>
                <tr>
                    <td><span style="width:100px;text-align:right;">权限备注：</span></td>
                    <td><input type="text" name="remark" id="remark" class="normal-input"></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button type="button" id="return" class="login-button">返回</button>
                        <button type="button" id="operation" class="login-button">提交</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </fieldset>
    </form>
</section>
</body>
</html>