<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/base.css">
    <link rel="stylesheet" href="/css/component.css">
    <link rel="stylesheet" href="/css/normal_table.css">
    <link rel="stylesheet" href="/css/userManager.css">
    <script src="/js/tableOperation.js" type="module"></script>
    <script src="/js/axios.min.js"></script>
    <script src="/js/webUtils.js" type="module"></script>
    <script src="/js/userManager.js" type="module"></script>
    <title>用户信息管理</title>
</head>

<body>
    <nav id="navigate">
        权限管理模块&nbsp;->&nbsp;用户信息管理页面&nbsp;<span id="operationMsg"></span>
    </nav>
    <section id="userTable">
        <nav id="navSearch">
        	<form action="user.servlet?param=init" method="post">
	            <input type="search" id="keyword" name="keyword" class="normal-input" value="${requestScope.page.keyword}" style="height: 35px;">
	            <button type="submit" class="login-button">查询</button>
            </form>
        </nav>
        <article id="main">
            <table class="norTable">
                <thead>
                    <tr>
                        <th><input type="checkbox" id="all"></th>
                        <th>用户名</th>
                        <th>昵称</th>
                        <th>密码</th>
                        <th>性别</th>
                        <th>email</th>
                        <th>电话</th>
                        <th>行政区划</th>
                        <th>爱好</th>
                        <th>生日</th>
                    </tr>
                </thead>
                <tbody id="norTableTbody">
                	<c:forEach items="${requestScope.page.list}" var="row">
                    <tr>
                        <td style="text-align: center"><input type="checkbox" data-oid="${row.get('id')}" class="cbxSelect"></td>
                        <td>${row.get("userName")}</td>
                        <td>${row.get("nickName")}</td>
                        <td>${row.get("password")}</td>
                        <td>
                        	<c:if test="${row.get('sex') eq 0}">null</c:if>
                        	<c:if test="${row.get('sex') eq 1}">男</c:if>
                        	<c:if test="${row.get('sex') eq 2}">女</c:if>
                        	</td>
                        <td>${row.get("email")}</td>
                        <td>${row.get("phone")}</td>
                        <td>${row.get("npName")}</td>
                        <td>${row.get("hobbyCode")}</td>
                        <td>${row.get("birthday")}</td>
                    </tr>
                    </c:forEach>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10">
                            <nav style="float: left;">
                                <!--<button id="btnAdd" class="login-button">新增</button>-->
                                <button id="btnDelete" class="login-button">删除</button>
                                <button id="btnUpdate" class="login-button">修改</button>
                                <button id="btnRoleUpdate" class="login-button">修改用户角色</button>
                            </nav>
                            <nav id="pageGroup" style="float:right">
                           		当前第<span id="currentPage">${requestScope.page.currentPage}</span>页&nbsp;
                                共${requestScope.page.totalRowNumber}行数据
                                每页${requestScope.page.pageNumber}行
                            	共<span id="totalPageNumber">${requestScope.page.totalPageNumber}</span>页&nbsp;

                            	&nbsp;
                                <c:if test="${requestScope.page.currentPage==1}">首页</c:if>
                            	<c:if test="${requestScope.page.currentPage!=1}"><a href="#" id="first">首页</a></c:if>
                            	&nbsp;
                            	<c:if test="${requestScope.page.currentPage==1}">上一页</c:if>
                            	<c:if test="${requestScope.page.currentPage!=1}"><a href="#" id="previous">上一页</a></c:if>
                            	&nbsp;
                                <select id="jumpPage" class="normal-input" style="width:50px;height:30px;">
                                	<c:forEach begin="1" end="${requestScope.page.totalPageNumber }" var="i">
                                		<c:if test="${i==requestScope.page.currentPage}">
                                			<option selected  value="${i }">${i }</option>
                                		</c:if>
                                		<c:if test="${i!=requestScope.page.currentPage}">
                                			<option value="${i }">${i }</option>
                                		</c:if>
                                	</c:forEach>
                                </select>
								&nbsp;
								<c:if test="${requestScope.page.currentPage==requestScope.page.totalPageNumber}">下一页</c:if>
								<c:if test="${requestScope.page.currentPage!=requestScope.page.totalPageNumber}">
                                <a href="#" id="next">下一页</a>
                                </c:if>
								&nbsp;
								<c:if test="${requestScope.page.currentPage==requestScope.page.totalPageNumber}">尾页</c:if>
								<c:if test="${requestScope.page.currentPage!=requestScope.page.totalPageNumber}">
                                <a href="#" id="last">尾页</a>
                                </c:if>
                            </nav>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </article>
    </section>
    <section id="userOperation" hidden>
    	<form action="user.servlet?param=doUpdate" method="post">
	        <fieldset style="border:0px;">
	        	<input type="hidden" name="userId" id="userId">
	        	<input type="hidden" name="userDetailId" id="userDetailId">
	            <legend hidden>用户操作</legend>
                <table>
                    <tbody>
                        <tr>
                            <td><span style="width:100px;text-align:right;">用户名：</span></td>
                            <td><input type="text" name="userName" id="userName" class="normal-input"></td>
                        </tr>
                        <tr>
                            <td><span style="width:100px;text-align:right;">昵称：</span></td>
                            <td><input type="text" name="nickName" id="nickName" class="normal-input"></td>
                        </tr>
                        <tr>
                            <td>密码：</td>
                            <td><input type="password" name="password" id="password" class="normal-input"></td>
                        </tr>
                        <tr>
                            <td>性别：</td>
                            <td><input type="radio" name="sex" value="1" id="man">男&nbsp;&nbsp;<input type="radio" name="sex" value="2" id="woman">女</td>
                        </tr>
                        <tr>
                            <td>email：</td>
                            <td><input type="text" name="email" id="email" class="normal-input"></td>
                        </tr>
                        <tr>
                            <td>电话：</td>
                            <td><input type="text" name="phone" id="phone" class="normal-input"></td>
                        </tr>
                        <tr>
                            <td>生日：</td>
                            <td><input type="date" name="birthday" id="birthday" class="normal-input"></td>
                        </tr>
                        <tr>
                            <td>爱好：</td>
                            <td><span id="hobbyInsertPoint"></span></td>
                        </tr>
                        <tr>
                            <td>来自于：</td>
                            <td><select id="province"></select>&nbsp;&nbsp;<select id="city" name="city"></select></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <button type="button" id="return" class="login-button">返回</button>
                                <button type="submit" id="userOperationAdd" class="login-button">提交</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
	        </fieldset>
        </form>
    </section>
    <section id="roleOperation" hidden>
	    <nav id="role_title">用户名称：<span id="userNameInsertPoint"></span></nav>
	    <form action="user.servlet?param=roleUpdate" method="post">
	    <aside id="section_roleUpdate">
	    	<input type="hidden" name="userId" id="roleOperationUserId">
	        <table class="roleTable">
	            <tbody id="roleInsertPoint">
	            <!-- 
	                <tr>
	                    <td class="module_level">
	                        <input type="checkbox" class="normalCheckbox"><span class="normal_Text">用户信息管理</span>&nbsp;
	                        <input type="checkbox" class="normalCheckbox"><span class="normal_Text">角色信息管理</span>&nbsp;
	                        <input type="checkbox" class="normalCheckbox"><span class="normal_Text">权限信息管理</span>
	                    </td>
	                </tr>
	            -->
	            </tbody>
	        </table>
	    </aside>
	    <nav id="buttonGroup" style="text-align: center">
	        <button type="button" id="roleOperationReturn" class="login-button">返回</button>
	        <button type="submit" id="roleOperationAdd" class="login-button">保存</button>
	    </nav>
	    </form>
    </section>
</body>
</html>