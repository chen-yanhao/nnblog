<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>登录</title>
    <link rel="stylesheet" href="/css/base.css">
    <link rel="stylesheet" href="/css/login.css">
    <link rel="stylesheet" href="/css/component.css">
    <script>
    	function deleteMsg(){
    		document.getElementById("errorMsg").innerHTML="";
    	}
        function selectCookie() {
            let o = document.getElementById("cookieGroup");
            let nav = document.getElementById("showCookie");
            if (o.hidden) {
                o.hidden = false;
                nav.classList.remove("change");
            } else {
                o.hidden = true;
                nav.classList.add("change");
            }
        }

        function init() {
            //监听绿色小按钮，当该按钮被点击时，执行selectCookie函数
            document.getElementById("showCookie").addEventListener("click", selectCookie, false);
            document.getElementById("userName").addEventListener("focus",deleteMsg,false);
            document.getElementById("password").addEventListener("focus",deleteMsg,false);
        }
        window.addEventListener("load", init, false); //监听网页加载事件，加载完成后执行init函数。
    </script>
</head>

<body>
    <section>
        <aside id="logo">
        <nav id="errorMsg">${requestScope.message}</nav>
        </aside>
        
        <article id="main">
            <form action="user.servlet?param=login" method="post">
                <fieldset>
                    <legend hidden>用户登录</legend>
                    <label for="userName" style="font-size:15px;">用户名：</label>
                    <input type="text" name="userName" id="userName" class="normal-input"><br>
                    <label for="password" style="font-size:15px;">密&nbsp;&nbsp;&nbsp;码：</label>
                    <input type="password" name="password" id="password" class="normal-input"><br>
                    <nav id="cookieGroup">
                        <input type="checkbox" name="isCookie" id="isCookie">
                        <label for="isCookie" style="font-size:15px;">一周之内免登录</label>
                    </nav>
                    <nav id="showCookie"></nav>
                    <nav id="buttonGroup">
                        <button type="submit" class="login-button">登录</button>
                        <button type="submit" class="login-button" id="btnRegister" formaction="register.jsp">注册</button>
                    </nav>
                </fieldset>
            </form>
        </article>
    </section>
</body>

</html>