export class TableOperation{
	constructor(){}
	//选择某行时，行头的复选框被选中
	selectRow(event) {
	    if (event.target.tagName.toLowerCase() == 'td') {
	        let input = event.target.parentNode.firstElementChild.firstElementChild;
	        //input.toggleAttribute("checked");	//使用该方法也可以，但操作时有debug出现，原因未知。
	        if(input.checked){
	        	input.checked=false;
	        }else{
	        	input.checked=true;
	        }
	        event.target.parentNode.classList.toggle("norTableCheckedRow");
	    }
	}
	//当表头的复选框被改变时执行的操作
	allSelect() {
	    let all = document.getElementById("all");
	    document.querySelectorAll(".cbxSelect").forEach(element => {
	        element.checked = all.checked;
	        if (all.checked) {
	            element.parentNode.parentNode.classList.add("norTableCheckedRow");
	        } else {
	            element.parentNode.parentNode.classList.remove("norTableCheckedRow");
	        }
	    });
	}
	//点击行中复选框时，该复选框所在的行变色。
	toggleInput(element){
	    element.addEventListener("change", () => {
	        element.parentNode.parentNode.classList.toggle("norTableCheckedRow");
	    });
	}
}
