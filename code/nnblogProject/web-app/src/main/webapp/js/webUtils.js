export class WebUtils{
	constructor(){}
	
    getDate(ms){
    	let date=new Date(ms);
    	let month=date.getMonth()+1;
        let day=date.getDate();
        let strDate=date.getFullYear()+"-";
        strDate+=10>month?"0"+month:month;
        strDate+="-";
        strDate+=day<10?"0"+day:day;
        return strDate;
    }
}