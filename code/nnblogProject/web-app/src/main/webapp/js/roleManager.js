import {WebUtils} from './webUtils.js';
import {TableOperation} from './tableOperation.js';
import {Page} from './page.js';


function doDelete(event) {
    let list = document.querySelectorAll(".cbxSelect");
    let parameter = "";
    if (window.confirm("你确定要进行删除操作吗？")) {
        [...list].filter(element => element.checked).forEach((element) => {
            if (parameter == '') {
                parameter += element.dataset.oid;
            } else {
                parameter += ',';
                parameter += element.dataset.oid;
            }
        });
        if (parameter == '') {
            alert("请选择要删除的数据行！");
        }
        window.location.href="role.servlet?param=delete&ids="+parameter;
    }
}

function doAdd() {
    document.getElementById("roleTable").hidden = true;
    document.getElementById("roleOperation").hidden = false;
    document.getElementById("operationMsg").innerHTML = "->&nbsp;新增角色";
}

function doUpdate() {}

function updateInit(response){
	let json=response.data;
	if(json.state=="ok"){
        document.getElementById("roleTable").hidden = true;
        document.getElementById("roleOperation").hidden = false;
        document.getElementById("operationMsg").innerHTML = "->&nbsp;修改角色";     
        document.getElementById("id").value=json.role.id;
        document.getElementById("roleName").value=json.role.roleName;
        document.getElementById("remark").value=json.role.remark;
	}else{
		alert("数据查询错误！");
	}
}

function updateSearch() {
    let list = document.querySelectorAll(".cbxSelect");
    let parameter = "";
    let count = 0;
    let fList = [...list].filter(element => element.checked);
    if (fList.length > 1) {
        alert("一只能修改一行数据！");
    } else if (fList.length == 0) {
        alert("请选择要修改的数据行！");
    } else {
        parameter = fList[0].dataset.oid;
        axios.get("role.servlet?param=update&id="+parameter)
        .then(response=>{
        	updateInit(response);
        })
        .catch(error=>{
        	console.log(error);
        });
    }
}
function permissionInit(response){
	document.getElementById("roleTable").hidden = true;
	document.getElementById("permissionOperation").hidden=false;
	document.getElementById("operationMsg").innerHTML = "->&nbsp;修改角色权限";  
	document.getElementById("roleNameInsertPoint").innerHTML=response.data.role.roleName;
	document.getElementById("roleId").value=response.data.role.id;
	let tbody=document.getElementById("permissionInsertPoint");
	let permissions=response.data.permissions;
	//构造权限选择结构
	permissions.filter(permission=>permission.pid==0).forEach(permission=>{
		let tr=document.createElement("tr");
		let td0=document.createElement("td");
		td0.setAttribute("class","module_level");
		let input0=document.createElement("input");
		input0.type="checkbox";
		input0.setAttribute("class","normalCheckbox");
		let span=document.createElement("span");
		span.innerHTML=permission.permissionName;
		span.setAttribute("class","normal_Text");
		td0.appendChild(input0);
		td0.appendChild(span);
		tr.appendChild(td0);
		
		let td1=document.createElement("td");
		td1.setAttribute("class","page_level");
		permissions.filter(element=>element.pid==permission.id).forEach(element=>{
			let input0=document.createElement("input");
			input0.type="checkbox";
			input0.setAttribute("class","normalCheckbox");
			input0.name="permissionIds";
			input0.value=element.id;
			if(element.checked){
				input0.checked=true;
			}
			let span=document.createElement("span");
			span.innerHTML=element.permissionName;
			span.setAttribute("class","normal_Text");	
			td1.appendChild(input0);
			td1.appendChild(span);
			tr.appendChild(td1);
		});
		tbody.appendChild(tr);
		
	});
}
function searchRole(){
    let list = document.querySelectorAll(".cbxSelect");
    let parameter = "";
    let count = 0;
    let fList = [...list].filter(element => element.checked);
    if (fList.length > 1) {
        alert("一只能修改一行数据！");
    } else if (fList.length == 0) {
        alert("请选择要修改的数据行！");
    } else {
        parameter = fList[0].dataset.oid;
        axios.get("permission.servlet?param=search&id="+parameter)
        .then(response=>{
        	permissionInit(response);
        })
        .catch(error=>{
        	console.log(error);
        });
    }	
}
function returnTable(){
	document.getElementById("roleTable").hidden = false;
	document.getElementById("permissionOperation").hidden=true;
	document.getElementById("operationMsg").innerHTML = "";
	document.getElementById("permissionInsertPoint").innerHTML="";
}

function init(){
	//normal table operation
	let tableOperation = new TableOperation();
	document.getElementById("norTableTbody").addEventListener("click", tableOperation.selectRow, false); //选择行操作
	//row checkbox input
	document.querySelectorAll(".cbxSelect").forEach((element) => {
		tableOperation.toggleInput(element);
	});
	//table header checkbox input
	document.getElementById("all").addEventListener("change", tableOperation.allSelect, false);
	
	//CRUD delete
	document.getElementById("btnDelete").addEventListener("click", doDelete, false);
	//CRUD add
	document.getElementById("btnAdd").addEventListener("click", doAdd, false);
	document.getElementById("operation").addEventListener("click",()=>{
		let text=document.getElementById("operationMsg").innerHTML;
		if(text.indexOf("新增")!=-1){
			document.getElementById("addOrUpdateForm").action="role.servlet?param=add";
		}
		if(text.indexOf("修改")!=-1){
			document.getElementById("addOrUpdateForm").action="role.servlet?param=doUpdate";	
		}
		document.getElementById("addOrUpdateForm").submit();	
	},false);
	//CRUD add return
	document.getElementById("return").addEventListener("click", () => {
	    document.getElementById("roleTable").hidden = false;
	    document.getElementById("roleOperation").hidden = true;
	    document.getElementById("operationMsg").innerHTML = "";
	}, false);
	//CRUD update
	document.getElementById("btnUpdate").addEventListener("click", updateSearch, false);
	
	//permisson update
	document.getElementById("btnPermission").addEventListener("click",searchRole,false);

	//return main page
	document.getElementById("returnTable").addEventListener("click",returnTable,false);
	//page
	let page=new Page();
	let url="role.servlet?param=init";
	document.getElementById("pageGroup").addEventListener("click",event=>page.pageOpation(event,url),false);
	document.getElementById("jumpPage").addEventListener("change",event=>page.pageOpation(event,url),false)
}
window.addEventListener("load",init,false);