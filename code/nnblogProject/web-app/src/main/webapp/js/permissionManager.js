import {WebUtils} from './webUtils.js';
import {TableOperation} from './tableOperation.js';
import {Page} from './page.js';

function doDelete(event) {
    let list = document.querySelectorAll(".cbxSelect");
    let parameter = "";
    if (window.confirm("你确定要进行删除操作吗？")) {
        [...list].filter(element => element.checked).forEach((element) => {
            if (parameter == '') {
                parameter += element.dataset.oid;
            } else {
                parameter += ',';
                parameter += element.dataset.oid;
            }
        });
        if (parameter == '') {
            alert("请选择要删除的数据行！");
        }else{
            window.location.href="permission.servlet?param=delete&ids="+parameter;
        }
    }
}

function doAdd() {
    fetch("permission.servlet?param=getPid").then(response=>response.json()).then(data=>{
        document.getElementById("permissionTable").hidden = true;
        document.getElementById("permissionOperation").hidden = false;
        document.getElementById("operationMsg").innerHTML = "->&nbsp;新增权限";
        let select=document.getElementById("pid");
        data.parentPermission.forEach(element=>{
        	let option=document.createElement("option");
        	option.value=element.id;
        	option.innerHTML=element.permissionName;
        	select.appendChild(option);
        });
    }).catch(error=>console.log(error));
}

function updateInit(response){
	let json=response.data;
	if(json.state=="ok"){
        document.getElementById("permissionTable").hidden = true;
        document.getElementById("permissionOperation").hidden = false;
        document.getElementById("operationMsg").innerHTML = "->&nbsp;修改权限";     
        document.getElementById("id").value=json.permission.id;
        let select=document.getElementById("pid");
        json.parentPermission.forEach(element=>{
        	let option=document.createElement("option");
        	option.value=element.id;
        	option.innerHTML=element.permissionName;
        	if(json.permission.pid==element.id){
        	    option.selected=true;
            }
        	select.appendChild(option);
        });
        document.getElementById("url").value=json.permission.url;
        document.getElementById("permissionName").value=json.permission.permissionName;
        document.getElementById("remark").value=json.permission.remark;
	}else{
		alert("数据查询错误！");
	}
}

function updateSearch() {
    let list = document.querySelectorAll(".cbxSelect");
    let parameter = "";
    let count = 0;
    let fList = [...list].filter(element => element.checked);
    if (fList.length > 1) {
        alert("一只能修改一行数据！");
    } else if (fList.length == 0) {
        alert("请选择要修改的数据行！");
    } else {
        parameter = fList[0].dataset.oid;
        axios.get("permission.servlet?param=update&id="+parameter)
        .then(response=>{
        	updateInit(response);
        })
        .catch(error=>{
        	console.log(error);
        });
    }
}


function init(){
	//normal table operation
	let tableOperation = new TableOperation();
	document.getElementById("norTableTbody").addEventListener("click", tableOperation.selectRow, false); //选择行操作
	//row checkbox input
	document.querySelectorAll(".cbxSelect").forEach((element) => {
		tableOperation.toggleInput(element);
	});
	//table header checkbox input
	document.getElementById("all").addEventListener("change", tableOperation.allSelect, false);
	
	//CRUD delete
	document.getElementById("btnDelete").addEventListener("click", doDelete, false);
	//CRUD add
	document.getElementById("btnAdd").addEventListener("click", doAdd, false);
	document.getElementById("operation").addEventListener("click",()=>{
		let text=document.getElementById("operationMsg").innerHTML;
		if(text.includes("新增")){
			document.getElementById("addOrUpdateForm").action="permission.servlet?param=add";
		}
		if(text.includes("修改")){
			document.getElementById("addOrUpdateForm").action="permission.servlet?param=doUpdate";	
		}
		document.getElementById("addOrUpdateForm").submit();	
	},false);
	//CRUD add return
	document.getElementById("return").addEventListener("click", () => {
	    document.getElementById("permissionTable").hidden = false;
	    document.getElementById("permissionOperation").hidden = true;
	    document.getElementById("operationMsg").innerHTML = "";
	}, false);
	//CRUD update
	document.getElementById("btnUpdate").addEventListener("click", updateSearch, false);
	
	//page
	let page=new Page();
	let url="permission.servlet?param=init";
	document.getElementById("pageGroup").addEventListener("click",event=>page.pageOpration(event,url),false);
	document.getElementById("jumpPage").addEventListener("change",event=>page.pageOpration(event,url),false)
}
window.addEventListener("load",init,false);