export class Page{
	constructor(){}
	pageOpration(event,url){
		let pageTag=event.target.id;
		let type=event.type;
		let currentPage=document.getElementById("currentPage").innerHTML;
		let keyword=document.getElementById("keyword").value;
		url+="&";
		if(keyword!=""){
			url+="keyword="+keyword+"&"
		}
		if("jumpPage"!=pageTag && "click"==type){
			switch(pageTag){
				case "next":currentPage++;break;
				case "first":currentPage=1;break;
				case "last":currentPage=document.getElementById("totalPageNumber").innerHTML;break;
				case "previous":currentPage--;break;
			}
		} 
		if("jumpPage"==pageTag && "change"==type){
			currentPage=document.getElementById("jumpPage").value;
		}
		window.location.href=url+"currentPage="+currentPage;
	}
}