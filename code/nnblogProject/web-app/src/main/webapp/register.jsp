<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>用户注册页面</title>
    <link rel="stylesheet" href="./css/register.css">
    <link rel="stylesheet" href="./css/base.css">
    <link rel="stylesheet" href="./css/component.css">
    <script src="./js/ajaxUtils.js" type="module"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="./js/register.js" type="module"></script>
</head>

<body>
    <section>
        <h1>用户信息注册</h1>
        <form action="user.servlet?param=reg" method="post">
            <fieldset>
                <legend hidden>用户注册</legend>
                <table>
                    <tbody>
                        <tr>
                            <td style="width:100px;"><label for="userName" class="text">用户名：</label></td>
                            <td><input type="text" name="userName" id="userName" placeholder="请输入英文或数字组合信息" class="normal-input"></td>
                            <td>
                                <nav id="userNameMsg">
                                    <div class="msgImage"></div>
                                    <div class="msgInfo"></div>
                                </nav>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="nickName" class="text">昵称：</label></td>
                            <td><input type="text" name="nickName" id="nickName" placeholder="可以输入中文信息" class="normal-input"></td>
                            <td>
                                <nav id="nickNameMsg">
                                    <div class="msgImage"></div>
                                    <div class="msgInfo"></div>
                                </nav>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="password" class="text">密码：</label></td>
                            <td><input type="password" name="password" id="password" placeholder="请输入密码" class="normal-input"></td>
                            <td>
                                <nav id="passwordMsg">
                                    <div class="msgImage"></div>
                                    <div class="msgInfo"></div>
                                </nav>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="rePassword" class="text">重复密码：</label></td>
                            <td><input type="password" name="rePassword" id="rePassword" placeholder="请重复输入密码" class="normal-input"></td>
                            <td>
                                <nav id="rePasswordMsg">
                                    <div class="msgImage"></div>
                                    <div class="msgInfo"></div>
                                </nav>
                            </td>
                        </tr>
                        <tr>
                            <td class="text">性别：</td>
                            <td colspan="2">
                                <input type="radio" name="sex" id="man" value="1" class="normalRadio">
                                <label for="man" style="margin-left:10px;vertical-align: 12px;" class="text">男</label>&nbsp;&nbsp;
                                <input type="radio" name="sex" id="woman" value="2" class="normalRadio">
                                <label for="woman" style="margin-left:10px;vertical-align: 12px;" class="text">女</label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="phone" class="text">手机号：</label></td>
                            <td><input type="text" name="phone" id="phone" placeholder="请输入电话号码" class="normal-input"></td>
                            <td>
                                <nav id="phoneMsg">
                                    <div class="msgImage"></div>
                                    <div class="msgInfo"></div>
                                </nav>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="email" class="text">Email：</label></td>
                            <td><input type="email" name="email" id="email" placeholder="请输入邮箱地址" class="normal-input"></td>
                            <td>
                                <nav id="emailMsg">
                                    <div class="msgImage"></div>
                                    <div class="msgInfo"></div>
                                </nav>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="birthday" class="text">生日：</label></td>
                            <td colspan="2"><input type="date" name="birthday" id="birthday" class="normal-input"></td>
                        </tr>
                        <tr>
                            <td class="text">爱好：</td>
                            <td colspan="2">
                            	<div id="hobbyInsertPonit"></div>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="from" class="text">来自于：</label></td>
                            <td colspan="2">
                                <select name="province" id="province" class="normal-input" style="height: 40px;"></select>&nbsp;&nbsp;
                                <select name="city" id="city" class="normal-input" style="height: 40px;"></select>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="remark" class="text">备注：</label></td>
                            <td colspan="2">
                                <textarea name="" cols="30" rows="8" placeholder="请输入备注信息" class="normal-input"></textarea>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>
            <nav colspan="3" style="text-align:center">
	            <button type="reset" class="login-button">重置</button>&nbsp;&nbsp;
	            <button type="submit" class="login-button">注册</button>
        	</nav>
        </form>

    </section>
</body>

</html>