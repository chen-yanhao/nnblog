package com.yuhf.web.utils;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLDecoder;

public class HttpMessageConvert {

    public static String reqJson(HttpServletRequest request){
        BufferedReader br;
        StringBuilder sb = new StringBuilder();
        String decode=null;
        try {
            br = new BufferedReader(new InputStreamReader((ServletInputStream) request.getInputStream()));
            String line = null;

            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            decode = URLDecoder.decode(new String(sb.toString().getBytes(), "utf-8"), "utf-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return decode;
    }
}
