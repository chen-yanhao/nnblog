package edu.yuhf.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;

import edu.yuhf.domain.NativePlace;
import edu.yuhf.service.NativePlaceServiceImpl;
import edu.yuhf.service.iface.NativePlaceService;


@WebServlet("/np.servlet")
public class NativePlaceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		NativePlaceService nativePlaceSerivce=new NativePlaceServiceImpl();
		
		String param=request.getParameter("param");
		String provinceCode=request.getParameter("provinceCode");
		
		PrintWriter out=response.getWriter();
		Map<String,Object> map=new HashMap<>();
		List<NativePlace> list=new ArrayList<>();

		switch(param){
			case "province":{
				list=nativePlaceSerivce.getProvince();
				map.put("result","ok");
				map.put("list", list);
				break;
			}
			case "cities":{
				list=nativePlaceSerivce.getCities(provinceCode);
				map.put("result","ok");
				map.put("list", list);
				break;
			}
			case "init":{

				break;
			}
			default:
				throw new IllegalStateException("Unexpected value: " + param);
		}
		String json=JSON.toJSONString(map);
		out.print(json);
		out.flush();
		out.close();
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
