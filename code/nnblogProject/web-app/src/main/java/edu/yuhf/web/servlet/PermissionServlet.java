package edu.yuhf.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;

import edu.yuhf.Constants;
import edu.yuhf.domain.Page;
import edu.yuhf.domain.Permission;
import edu.yuhf.domain.Role;
import edu.yuhf.domain.Search;
import edu.yuhf.service.PermissionServiceImpl;
import edu.yuhf.service.RoleServiceImpl;
import edu.yuhf.service.iface.PermissionService;
import edu.yuhf.service.iface.RoleService;


@WebServlet("/permission.servlet")
public class PermissionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PermissionService permissionService=new PermissionServiceImpl();
		RoleService roleService=new RoleServiceImpl();
		
		String param=request.getParameter("param");
		String keyword=null==request.getParameter("keyword")?"":request.getParameter("keyword");
		String menuLevel=null==request.getParameter("menuLevel")?"-1":request.getParameter("menuLevel");
		String pid=request.getParameter("pid");
		String roleName=request.getParameter("permissionName");
		String url=request.getParameter("url");
		String remark=request.getParameter("remark");
		
		String ids=request.getParameter("ids");
		String id=null==request.getParameter("id")?"":request.getParameter("id");
		
		String currentPage=null==request.getParameter("currentPage")?"1":request.getParameter("currentPage");
		
		PrintWriter out=response.getWriter();
		
		Map<String,Object> map=new HashMap<>();
		if(Constants.SEARCH.equals(param)) {
			Optional<Role> optional=roleService.getRoleById(Integer.valueOf(id));
			optional.ifPresent(role->{
				List<Permission> list=permissionService.getPermissionsByRoleId(role.getId());
				map.put("state","ok");
				map.put("role", role);
				map.put("permissions",list);
			});
			optional.orElseGet(()->{
				map.put("state", "fail");
				return new Role();
			});
		}
		if("doUpdate".equals(param)) {
			Permission permission=new Permission(Integer.valueOf(id),Integer.valueOf(pid),roleName,url,remark);
			if(permissionService.setPermission(permission)) {
				out.print("<script>alert('更新成功');window.location.href='permission.servlet?param=init'</script>");
			}else {
				out.print("<script>alert('更新失败');window.location.href='permission.servlet?param=init'</script>");
			}
		}
		if("update".equals(param)) {
			Permission permission=permissionService.getPermissionById(Integer.valueOf(id));
			List<Permission> list=permissionService.getParentPermission();
			if(null!=permission) {
				map.put("state","ok");
				map.put("permission",permission);
				map.put("parentPermission",list);
			}else {
				map.put("state","fail");
			}
		}
		if("init".equals(param)) {
			Search search=new Search();
			search.setKeyword(keyword);
			search.setMenuLevel(Integer.valueOf(menuLevel));
			Page<List<Permission>> page=new Page<>(Integer.valueOf(currentPage));
			page.setSearch(search);
			
			permissionService.getPermission(page);
			request.setAttribute("page", page);
			request.getRequestDispatcher("views/permissionManager.jsp").forward(request, response);
		}
		if("delete".equals(param)) {
			if(permissionService.removePermission(ids)) {
				out.print("<script>alert('删除成功');window.location.href='permission.servlet?param=init'</script>");
			}else {
				out.print("<script>alert('删除失败');window.location.href='permission.servlet?param=init'</script>");
			}
		}
		if("add".equals(param)) {
			Permission permission=new Permission(0,Integer.valueOf(pid),roleName,url,remark);
			if(permissionService.addPermission(permission)) {
				out.print("<script>alert('添加成功');window.location.href='permission.servlet?param=init'</script>");
			}else {
				out.print("<script>alert('添加失败');window.location.href='permission.servlet?param=init'</script>");
			}
		}
		if("getPid".equals(param)) {
			List<Permission> list=permissionService.getParentPermission();
			if(null!=list) {
				map.put("state","ok");
				map.put("parentPermission",list);
			}else {
				map.put("state","fail");
			}
		}
		out.print(JSON.toJSONString(map));
		out.flush();
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
