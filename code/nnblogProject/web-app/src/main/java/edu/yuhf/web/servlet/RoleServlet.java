package edu.yuhf.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;

import edu.yuhf.Constants;
import edu.yuhf.domain.Page;
import edu.yuhf.domain.Role;
import edu.yuhf.domain.User;
import edu.yuhf.service.PermissionRoleServiceImpl;
import edu.yuhf.service.RoleServiceImpl;
import edu.yuhf.service.UserServiceImpl;
import edu.yuhf.service.iface.PermissionRoleService;
import edu.yuhf.service.iface.RoleService;
import edu.yuhf.service.iface.UserService;


@WebServlet("/role.servlet")
public class RoleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RoleService roleService=new RoleServiceImpl();
		UserService userService=new UserServiceImpl();
		PermissionRoleService permissionRoleService=new PermissionRoleServiceImpl();
		
		String param=request.getParameter("param");
		String keyword=null==request.getParameter("keyword")?"":request.getParameter("keyword");
		String roleName=request.getParameter("roleName");
		String remark=request.getParameter("remark");
		
		String roleId=request.getParameter("roleId");
		String[] permissionIds=null==request.getParameterValues("permissionIds")?new String[0]:request.getParameterValues("permissionIds");
		
		String ids=request.getParameter("ids");
		String id=request.getParameter("id");
		
		String currentPage=null==request.getParameter("currentPage")?"1":request.getParameter("currentPage");
		
		PrintWriter out=response.getWriter();
		
		Map<String,Object> map=new HashMap<>();
		
		if(Constants.SEARCH.equals(param)) {
			Optional<User> optional=userService.getUser(Integer.valueOf(id));
			optional.ifPresent(element->{
				List<Role> list=roleService.getRolesByUserId(Integer.valueOf(id));
				map.put("state", "ok");
				map.put("user",optional.orElse(new User()));
				map.put("list", list);
			});
			optional.orElseGet(()->{
				map.put("state","error");
				map.put("message","用户没有找到！");
				return new User();
			});
		}
		if(Constants.DO_UPDATE.equals(param)) {
			Role role=new Role(Integer.valueOf(id),roleName,remark);
			if(roleService.setRole(role)) {
				out.print("<script>alert('更新完成');window.location.href='role.servlet?param=init'</script>");
			}else {
				out.print("<script>alert('更新失败');window.location.href='role.servlet?param=init'</script>");
			}
		}
		if(Constants.UPDATE.equals(param)) {
			Optional<Role> optional=roleService.getRoleById(Integer.valueOf(id));
			optional.ifPresent((role)->{
				map.put("state","ok");
				map.put("role",role);
			});
			optional.orElseGet(()->{
				map.put("state","fail");
				return new Role();
			});
		}
		if("perUpdate".equals(param)) {
			if(permissionRoleService.setPermissionRole(Integer.valueOf(roleId), permissionIds)) {
				out.print("<script>alert('成功');window.location.href='role.servlet?param=init'</script>");
			}else {
				out.print("<script>alert('失败');window.location.href='role.servlet?param=init'</script>");
			}
		}
		if(Constants.INIT.equals(param)) {
			Page<List<Role>> page=new Page<>(Integer.valueOf(currentPage));
			page.setKeyword(keyword);
			
			roleService.getRoles(page);
			request.setAttribute("page", page);
			request.getRequestDispatcher("views/roleManager.jsp").forward(request, response);
		}
		if(Constants.DELETE.equals(param)) {
			if(roleService.removeRole(ids)) {
				out.print("<script>alert('删除成功');window.location.href='role.servlet?param=init'</script>");
			}else {
				out.print("<script>alert('删除失败');window.location.href='role.servlet?param=init'</script>");
			}
		}
		if(Constants.ADD.equals(param)) {
			Role role=new Role(0,roleName,remark);
			if(roleService.addRole(role)) {
				out.print("<script>alert('添加角色成功');window.location.href='role.servlet?param=init'</script>");
			}else {
				out.print("<script>alert('添加角色失败');window.location.href='role.servlet?param=init'</script>");
			}
		}
		out.print(JSON.toJSONString(map));
		out.flush();
		out.close();
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
