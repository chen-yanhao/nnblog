package edu.yuhf;

public class Constants {
	
	public static final String BASE_PATH="http://127.0.0.1:9000";
	
	public static final int PAGE_NUMBER=5;

	public static final String LOGIN="login";
	public static final String REGISTER="reg";
	public static final String INIT="init";
	public static final String ADD="add";
	public static final String DELETE="delete";
	public static final String UPDATE="update";
	public static final String DO_UPDATE="doUpdate";
	public static final String SEARCH="search";

}
