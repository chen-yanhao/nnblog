package edu.yuhf.web.servlet;

import com.alibaba.fastjson.JSON;
import edu.yuhf.service.HobbyServiceImpl;
import edu.yuhf.service.iface.HobbyService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "HobbyServlet",urlPatterns = "/hobby.servlet")
public class HobbyServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HobbyService hobbyService=new HobbyServiceImpl();
        Map<String,Object> map=new HashMap();
        PrintWriter out=response.getWriter();
        map.put("hobbies",hobbyService.showHobbies());
        String jsonString= JSON.toJSONString(map);
        out.print(jsonString);
        out.flush();
        out.close();
    }
}
